package algo.rhopomdp_reward_bprime.random;

import java.util.List;

import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * allow to execute random policy
 *
 */
public class ExecRhoPOMDPRandom<S, A, O> implements ExecuteRhoPOMDP<S, A, O> {

	/**
	 * problem to solve
	 */
	RhoPOMDP<S, A, O> problem;

	/**
	 * create an objet to execute random algo
	 * 
	 * @param pb problem to solve
	 */
	public ExecRhoPOMDPRandom(RhoPOMDP<S, A, O> pb) {
		this.problem = pb;
	}

	@Override
	public void initializeBelief(Belief<S> b) {
		// no use
	}

	@Override
	/**
	 * return selected action
	 */
	public A getAction() {
		List<A> actions = problem.allAction();
		int hasard = RandomGen.randomInt(actions.size());
		return actions.get(hasard);
	}

	@Override
	public void executeAction(A action, O observation) {
		// no internal update of algorithm 
	}

	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public String getInformation() {
		return "";
	}

}
