package algo.rhopomdp_reward_bprime.rhoBeliefUCT.rollout;

import algo.pomdp.pomcp.Bag;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * rollout policy for rho-beliefUCT
 * <p>
 * This rollout does not execute any policy but returns a constant value
 * 
 * @author vthomas
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class RolloutBeliefConstant<S, A, O> implements RolloutBeliefUCT<S, A, O> {

	/**
	 * constant value to return
	 */
	double val;

	/**
	 * creates a constant rollout
	 * 
	 * @param cst constant to return whenever rollout is needed
	 */
	public RolloutBeliefConstant(double cst) {
		this.val = cst;
	}

	@Override
	/**
	 * return rollout value
	 */
	public double rollout(Bag<S> newParticles, int depth, RhoPOMDP<S, A, O> pb) {
		return this.val;
	}

}
