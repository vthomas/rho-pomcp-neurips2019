package algo.rhopomdp_reward_bprime.rhoBeliefUCT.rollout;

import java.util.List;

import algo.pomdp.pomcp.Bag;
import algo.rhopomdp_reward_bprime.rhoBeliefUCT.RhoBeliefUCT;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * random rollout for rho-beliefUCT
 * <p>
 * actions are randomly selected.
 * 
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class RolloutBeliefRandom<S, A, O> implements RolloutBeliefUCT<S, A, O> {

	/**
	 * creates random rollout
	 */
	public RolloutBeliefRandom() {
	}

	@Override
	/**
	 * execute random action, but need to compute belief state to assess rewards
	 */
	public double rollout(Bag<S> bag, int depth, RhoPOMDP<S, A, O> pb) {
		// if deep enough, cut rollout
		if (Math.pow(pb.getGamma(), depth) < RhoBeliefUCT.EPSILON) {
			return 0;
		}

		// sample transition
		Belief<S> initialB = new Belief<S>(bag);

		// sample action
		List<A> actions = pb.allAction();
		A action = actions.get(RandomGen.randomInt(actions.size()));

		// sample initial state, Sprime and observation
		S initialS = initialB.sample();
		S Sprime = pb.transition(initialS, action).sample();
		O obs = pb.observation(initialS, action, Sprime).sample();

		// need to compute new belief after action and received observation
		Bag<S> afterBag = RhoBeliefUCT.computeBelief(bag, action, obs, pb);
		Belief<S> after = new Belief<S>(afterBag);

		// estimate reward
		double reward = pb.reward(initialB, action, after);
		// recursive call
		return reward + pb.getGamma() * this.rollout(afterBag, depth + 1, pb);
	}

}
