package algo.rhopomdp_reward_bprime.myopic;

import java.util.HashMap;
import java.util.Map;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * a myopic (information greedy) search.
 * 
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class MyopicSearch<S, A, O> {

	/**
	 * rhoPOMDP problem
	 */
	RhoPOMDP<S, A, O> problem;

	/**
	 * create a myopic algorithm
	 * 
	 * @param pb problem to solve
	 */
	public MyopicSearch(RhoPOMDP<S, A, O> pb) {
		this.problem = pb;
	}

	/**
	 * get best action for belief b by comparing myopic value (immediate reward)
	 * 
	 * @param b initial belief
	 * @return best action for belief b
	 */
	public A getMyopicAction(Belief<S> b) {
		// max next value and best action
		double maxNextValue = -Double.MAX_VALUE;
		A bestAction = null;

		// assess each action
		for (A action : this.problem.allAction()) {
			// estimate expectedActionValue
			double actionVal = 0;

			// for each observation
			for (O obs : this.problem.allObs()) {
				// save arrival state probabilities
				// P(S',O) = P(O|s,s',a) P(s'|a,s) P(s)
				Map<S, Double> pS = getDistSPrimeObs(b, action, obs);

				// compute probaObs from Ps probaObs = sum s' P(s',O)
				// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s,s'|a)
				// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s'|a,s) P(s)
				double probaObs = 0.;

				for (S sPrime : pS.keySet()) {
					probaObs = probaObs + pS.get(sPrime);
				}

				// if observation is possible
				if (probaObs > 0) {
					// probaObs is P(O|a) (computed)
					// ps is P(s',o|a)
					// estimate immediate reward (myopic)

					// compute bPrime
					Belief<S> bPrime = new Belief<>();
					// P(s'|o) = P(s',o|a) / P(o|a)
					for (S sPrime : pS.keySet()) {
						bPrime.put(sPrime, pS.get(sPrime) / probaObs);
					}

					// compute r
					double r = this.problem.reward(b, action, bPrime);

					// update actionValue
					actionVal += probaObs * r;
				}
			}
			// action value computed
			if (actionVal > maxNextValue) {
				maxNextValue = actionVal;
				bestAction = action;
			}
		}
		return bestAction;
	}

	/**
	 * compute P(s',O) based on P(s), P(s'|s) and P(O|s')
	 * 
	 * @param b      belief at time t b(s)
	 * @param action perfomed action
	 * @param obs    received observation
	 * @return P(s',O)
	 */
	private Map<S, Double> getDistSPrimeObs(Belief<S> b, A action, O obs) {
		Map<S, Double> pS = new HashMap<>();

		// for each initial state from belief
		for (S s : b.keySet()) {
			// belief s
			double bs = b.get(s);

			// get transition from s
			Distribution<S> transition = this.problem.transition(s, action);

			// for each reachable S'
			for (S sPrime : transition) {
				// p(o|s,s',a)
				double probaO = this.problem.observation(s, action, sPrime).getProba(obs);

				// p(s'|s,a)
				double probaT = transition.getProba(sPrime);

				// compute contribution to P(s') from b(s) = p(s'|s) * b(s) * p(O|s')
				double contrib = probaO * probaT * bs;

				// if map(S') not exist, create contribution
				if (!pS.containsKey(sPrime))
					pS.put(sPrime, contrib);
				else
					// add contribution to map(S')
					pS.put(sPrime, pS.get(sPrime) + contrib);

			}
		}
		return pS;
	}

	/**
	 * update belief according to action and observation
	 * 
	 * @param b      current belief
	 * @param action current action
	 * @param obs    current observation
	 * @return belief on next step
	 */
	public Belief<S> updateBelief(Belief<S> b, A action, O obs) {

		// save arrival state probabilities
		// P(S',O) = P(O|s,s',a) P(s'|a,s) P(s)
		Map<S, Double> pS = getDistSPrimeObs(b, action, obs);

		// compute probability of observation
		// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s,s'|a)
		// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s'|a,s) P(s)
		double proba = 0;
		for (S sPrime : pS.keySet()) {
			proba = proba + pS.get(sPrime);
		}

		// if observation is possible
		if (proba > 0) {
			// proba is P(O|a) (computed)
			// ps is P(s',o|a)
			// estimate immediate reward (myopic)

			// compute bPrime
			Belief<S> bPrime = new Belief<>();
			// P(s'|o) = P(s',o|a) / P(o|a)
			for (S sPrime : pS.keySet()) {
				bPrime.put(sPrime, pS.get(sPrime) / proba);
			}
			return bPrime;
		} else {
			// impossible observation
			throw new Error("Impossible Observation");
		}

	}

	/********** COMMON METHOD FROM RhoPOMDPAlgo **********/

	public A search(Belief<S> belief) {
		return this.getMyopicAction(belief);
	}

}
