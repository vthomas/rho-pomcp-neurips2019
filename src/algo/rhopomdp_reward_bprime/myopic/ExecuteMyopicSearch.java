package algo.rhopomdp_reward_bprime.myopic;

import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * class to execute myopic algorithm step by step
 *
 * @param <S> state space 
 * @param <A> action space
 * @param <O> observation space
 */
public class ExecuteMyopicSearch<S, A, O> implements ExecuteRhoPOMDP<S, A, O> {

	/**
	 * current belief
	 */
	private Belief<S> currentBelief;

	/**
	 * current myopic algorithm
	 */
	private MyopicSearch<S, A, O> myopic;

	/**
	 * create executer of myopic search (encapsulates myopic Search algorithm)
	 */
	public ExecuteMyopicSearch(RhoPOMDP<S, A, O> pb) {
		this.myopic = new MyopicSearch<>(pb);
	}

	@Override
	/**
	 * initialize current belief as initial one
	 */
	public void initializeBelief(Belief<S> b) {
		this.currentBelief = b;
	}

	@Override
	/**
	 * use myopic to determine best action from current belief
	 */
	public A getAction() {
		return this.myopic.search(this.currentBelief);
	}

	@Override
	/**
	 * update current belief by using known model
	 */
	public void executeAction(A action, O observation) {
		this.currentBelief = this.myopic.updateBelief(this.currentBelief, action, observation);
	}

	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public String getInformation() {
		return "";
	}

}
