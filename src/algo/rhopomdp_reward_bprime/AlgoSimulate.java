package algo.rhopomdp_reward_bprime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.BeliefUpdate;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import tools.RandomGen;

/**
 * Allow to launch algorithm and simulate the result
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class AlgoSimulate<S, A, O> {

	/**
	 * CONSTANT PRINT SCREEN
	 */
	public static boolean PRINT_VALUES = false;
	public static boolean PRINT_STATISTICS = false;

	/**
	 * RhoPOMDP algorithm to use
	 */
	ExecuteRhoPOMDP<S, A, O> algorithm;

	/**
	 * current rho pomdp
	 */
	private RhoPOMDP<S, A, O> rhopb;

	/**
	 * current seed
	 */
	private long seed = -1;

	/**
	 * 
	 * creation of a simulation and tree search algorithm
	 * 
	 * @param b  the initial belief
	 * @param pb the problem
	 */
	public AlgoSimulate(ExecuteRhoPOMDP<S, A, O> algo, RhoPOMDP<S, A, O> pb) {
		this.algorithm = algo;
		this.rhopb = pb;
	}

	/**
	 * launch several simulations
	 * 
	 * @param nbSimulations number of episodes
	 * @param nbAction      number of actions by episode
	 * @param b0            initial belief
	 */
	public void launchSimulations(int nbSimulations, int nbActions, Belief<S> b0) {
		// generate new seed
		this.seed = RandomGen.getSeed();

		// store performances
		List<Double> perfs = new ArrayList<>();

		System.out.println("num episode;initialS;performance;duration(ms);" + this.algorithm.getDescription() + "seed="
				+ this.seed + ";");

		// repeat episode
		for (int i = 0; i < nbSimulations; i++) {
			// for each episode
			double res = launchOneSimulation(i, nbActions, b0);
			perfs.add(res);
		}

		System.out.println("\n");

		// write more information
		if (PRINT_STATISTICS) {
			// average
			double perfSum = 0.;
			for (Double d : perfs) {
				perfSum += d;
			}
			double average = perfSum / perfs.size();
			System.out.println("  - average : " + average);

			// variance
			double variance = 0.;
			for (Double d : perfs) {
				double dist = average - d;
				variance += dist * dist;
			}
			variance = variance / perfs.size();
			System.out.println("  - variance: " + variance);

			// standard deviation
			double deviation = Math.sqrt(variance);
			System.out.println("  - std deviation: " + deviation);

			// standard error
			double stdError = deviation / Math.sqrt(perfs.size() - 1);
			System.out.println("  - std error: " + stdError);

			// interval
			System.out.println("  - interval: [" + (average - stdError) + "," + (average + stdError) + "]");
		}
	}

	/**
	 * launch one simulation with nbActions
	 * 
	 * @param nbActions
	 */
	private double launchOneSimulation(int num, int nbActions, Belief<S> b0) {

		// sample trueState
		S trueState = b0.sample();

		// launch one simulation with true state
		return launchOneSimulationTrueState(num, nbActions, b0, trueState);
	}

	/**
	 * launch one simulation with a true state
	 * 
	 * @param num       indice of simulation
	 * @param nbActions number of actions
	 * @param b0        initial belief
	 * @param trueState true state
	 * @return performance of this simulation
	 */
	public double launchOneSimulationTrueState(int num, int nbActions, Belief<S> b0, S trueState) {
		S intialState = trueState;

		// current belief
		Belief<S> currentBelief = b0;

		// store beginning time (for duration)
		long startTime = System.currentTimeMillis();

		// performance criteria
		double performance = 0;
		double gamma = this.rhopb.getGamma();

		// reinitialize policy
		this.algorithm.initializeBelief(b0);
		if (PRINT_VALUES)
			System.out.println(b0);

		// stocke information
		String info = null;

		// execute episode
		for (int it = 0; it < nbActions; it++) {
			if (PRINT_VALUES)
				System.out.println("-iteration: " + it);

			A action = this.algorithm.getAction();
			if (PRINT_VALUES)
				System.out.println("\t action: " + action);

			// simulate process
			S Sprime = this.rhopb.transition(trueState, action).sample();
			if (PRINT_VALUES)
				System.out.println("\t SPrime: " + Sprime);

			// simulate observation
			O obs = this.rhopb.observation(trueState, action, Sprime).sample();
			if (PRINT_VALUES)
				System.out.println("\t Obs: " + obs);

			// update current belief
			BeliefUpdate<S, A, O> updateB = new BeliefUpdate<S, A, O>(this.rhopb);
			Belief<S> nextBelief = updateB.updateBelief(currentBelief, action, obs);
			if (PRINT_VALUES) {
				DecimalFormat dc = new DecimalFormat("#.##");
				System.out.println("\t * new Belief *\n" + nextBelief.getString(dc));
			}

			// receive reward
			double reward = this.rhopb.reward(currentBelief, action, nextBelief);
			if (PRINT_VALUES)
				System.out.println("\t reward: " + reward);
			performance = performance + Math.pow(gamma, it) * reward;

			// save first information
			if (it == 0)
				info = this.algorithm.getInformation();

			// update policy
			this.algorithm.executeAction(action, obs);

			// update trueState
			trueState = Sprime;
			currentBelief = nextBelief;
		}

		long tLength = System.currentTimeMillis() - startTime;
		System.out.println(num + ";" + intialState + ";" + performance + ";" + tLength + ";" + info + seed);

		return performance;
	}

}
