package algo.rhopomdp_reward_bprime.rhopomcp.rollout;

import algo.pomdp.pomcp.Bag;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * constant rollout policy for rhoPOMCP.
 * <p>
 * This rollout does not execute any policy but returns a constant value as
 * estimate
 * 
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class RolloutConstant<S, A, O> implements Rollout<S, A, O> {

	/**
	 * value returned as value estimate
	 */
	double valEstimate;

	/**
	 * create a constant rollout policy
	 * 
	 * @param cst value estimate to return
	 */
	public RolloutConstant(double cst) {
		this.valEstimate = cst;
	}

	@Override
	public double rollout(Bag<S> newParticles, int depth, RhoPOMDP<S, A, O> pb) {
		// return constant value
		return this.valEstimate;
	}

}
