package algo.rhopomdp_reward_bprime.rhopomcp.rollout;

import algo.pomdp.pomcp.Bag;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * rollout for rhoPOMCP algorithm
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public interface Rollout<S, A, O> {

	/**
	 * compute rollout value from problem
	 * 
	 * @param newParticles particles bag (belief estimate) from which executing
	 *                     rollout
	 * @param depth        current depth
	 * @param pb           problem to solve
	 * @return value estimate from rollout execution
	 */
	double rollout(Bag<S> newParticles, int depth, RhoPOMDP<S, A, O> pb);

}
