package algo.rhopomdp_reward_bprime.rhopomcp.beliefGen;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * generate particles of small bag beta_{t+1} according to rejection sampling.
 * @author vthomas
 *
 * @param <S>
 * @param <A>
 * @param <O>
 */
public class BeliefGenerationReject<S, A, O> implements BeliefGeneration<S, A, O> {

	/**
	 * rho-pomdp used for simulation/generation
	 */
	RhoPOMDP<S, A, O> prob;

	/**
	 * number of compatible states to generate each time
	 */
	int nbWishedStates;

	/**
	 * build a beliefgeneration reject
	 * 
	 * @param pb       rhopomdp problem to use
	 * @param nbWished number of compatible states to generate
	 */
	public BeliefGenerationReject(RhoPOMDP<S, A, O> pb, int nbWished) {
		this.prob = pb;
		this.nbWishedStates = nbWished;
	}

	@Override
	public Bag<S> generate(NodeBeliefPOMCP<S, A, O> h,Bag<S> previousBag, S previousS, A action, O obs, S STrajectory) {
		// number of iteration (just to measure efficiency of sampling)
		int nbCompatible = 0;

		// belief to sample from
		Belief<S> belief = new Belief<S>(previousBag);

		// generated belief
		Bag<S> generatedBag = new Bag<>();

		// while not enough compatible generated states
		while (nbCompatible < this.nbWishedStates) {

			// sample state from belief
			S state = belief.sample();

			// get distribution of sprime from state
			Distribution<S> spDist = this.prob.transition(state, action);

			// sample sprime and o from s
			S sPrime = spDist.sample();

			// sample observation
			O newObs = this.prob.observation(state, action, sPrime).sample();

			// if o compatible with what has happened
			if (newObs.equals(obs)) {
				// add Sprime to bag
				generatedBag.addToBag(sPrime, 1);
				// increment number of compatible generated states
				nbCompatible++;
			}
			// else sampling is rejected
		}

		// add state obtained during trajectory to bag
		generatedBag.addToBag(STrajectory, 1);

		// return generated belief
		return generatedBag;
	}

	@Override
	public Bag<S> generateInitialBag(S state, Belief<S> initialBelief) {
		// create empty bag
		Bag<S> bag = new Bag<>();

		// add state s to the bag
		bag.addToBag(state, 1.0);

		// add sampled states from initialbelief (nbWished times)
		for (int i = 0; i < this.nbWishedStates; i++) {
			S sampledS = initialBelief.sample();
			bag.addToBag(sampledS, 1);
		}

		return bag;
	}

}
