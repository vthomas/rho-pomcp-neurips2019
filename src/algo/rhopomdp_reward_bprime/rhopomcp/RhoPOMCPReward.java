package algo.rhopomdp_reward_bprime.rhopomcp;

import algo.pomdp.pomcp.Bag;
import algo.pomdp.pomcp.LogDot;
import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.BeliefGeneration;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.Rollout;
import algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator.ValueEstimate;
import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * Rho-POMCP algorithm (based on POMCP) with changes for belief propagation
 * <p>
 * uses Nodes from pomcp
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class RhoPOMCPReward<S, A, O> {

	/**
	 * parameters of rhoPOMCP
	 */
	public static final double EPSILON = 0.01;

	/**
	 * to print values
	 */
	public static boolean PRINT_VALUES = false;
	static boolean PRINT_IT = false;
	public static boolean PRINT_DEBUG_TREE = false;

	/**
	 * problem to solve
	 */
	RhoPOMDP<S, A, O> pb;

	/**
	 * root of the tree
	 */
	public NodeBeliefPOMCP<S, A, O> root;

	/**
	 * initial belief
	 */
	Belief<S> initialBelief;

	//////////// rho POMCP parameters /////////////////////

	/**
	 * UCB constant
	 */
	private double UCB_C;

	/**
	 * BeliefGenerator used for propagating beliefs
	 */
	BeliefGeneration<S, A, O> generatorB;

	/**
	 * rollout used
	 */
	private Rollout<S, A, O> rollout;

	/**
	 * value estimator
	 */
	private ValueEstimate<S, A, O> estimate = null;

	//////////// End of rho POMCP parameters /////////////////////

	/**
	 * build rhoPOMCP algorithm
	 * 
	 * @param prob      problem to solve
	 * @param nbDescent number of descents for each pomcp execution
	 * @param gen       way to generate new beliefs to add through descent
	 * @param ucbCst    UCB constant
	 * @param keep      keep subtree when executing action
	 */
	public RhoPOMCPReward(RhoPOMDP<S, A, O> prob, Belief<S> initB, BeliefGeneration<S, A, O> gen,
			Rollout<S, A, O> rollout, ValueEstimate<S, A, O> estimate, double ucbCst) {
		this.pb = prob;
		this.initialBelief = initB;

		// initialize ucb constant
		this.UCB_C = ucbCst;

		// initialize beliefGenerator and rollout
		this.generatorB = gen;
		this.rollout = rollout;
		this.estimate = estimate;

		// build tree root
		this.root = new NodeBeliefPOMCP<>();
		this.root.initialize(pb.allAction());

		// initialise belief root
		Bag<S> b0 = this.root.getBag();
		for (S s : this.initialBelief.keySet())
			b0.addToBag(s, this.initialBelief.get(s));

		// FIXME need rollout for root node (even if useseless for decision) ???
		this.estimate.initializeNode(this.root, 0);
	}

	/**
	 * launch a search with a specified number of descents from current root node
	 * (assuming it contains the corresponding belief)
	 */
	public A search(int nbDescents) {
		// create a belief from root particles
		Belief<S> rootBelief = new Belief<>(root.getBag());

		if (PRINT_DEBUG_TREE) {
			System.out.println("XXX\n\n");
			LogDot<S, A, O> log = new LogDot<>(root);
			System.out.println(log.toDot(3));
			System.out.println("XXX\n\n");
		}

		// do specified number of descents
		for (int i = 0; i < nbDescents; i++) {
			if (PRINT_IT)
				System.out.println("****** iteration " + i);

			oneDescent(rootBelief);

			if (PRINT_DEBUG_TREE) {
				System.out.println("XXX\n\n");
				LogDot<S, A, O> log = new LogDot<>(root);
				System.out.println(log.toDot(5));
				System.out.println("XXX\n\n");
			}
		}

		// return best action
		return this.getBestAction();
	}

	/**
	 * launch one descent
	 * 
	 * @param rootBelief initial belief
	 * @param i          iteration unber
	 */
	public void oneDescent(Belief<S> rootBelief) {

		// sample state from belief
		S state = rootBelief.sample();

		// generate initial bag (WITH initial state)
		Bag<S> bag = this.generatorB.generateInitialBag(state, rootBelief);

		// simulate from state
		this.simulate(state, bag, this.root, 0);
	}

	/**
	 * make a simulation (descent) in MCTS manner (POMCP)
	 * 
	 * @param state sampled state
	 * @param h     horizon (max_depth)
	 * @param depth current depth
	 * @return value of simulation from state
	 */
	private double simulate(S state, Bag<S> beta, NodeBeliefPOMCP<S, A, O> h, int depth) {
		// XXX almost duplicate from POMCP

		// if enough depth, stop recursion
		double gamma = this.pb.getGamma();
		if (Math.pow(gamma, depth) < EPSILON) {
			// System.out.println("end of search : gamma(" + gamma + ")^depth(" + depth + ")
			// < EPSILON (" + EPSILON + ")");
			// return
			return 0;
		}

		// if h not in T (POMCP) ==> translated as if nbVisit = -1
		if (h.getVisit() == -1) {
			// visit to 0
			h.addVisit();

			// build actionNode children
			h.initialize(pb.allAction());

			// compute rollout
			double rollout2 = this.rollout(beta, depth);

			// initialize belief node value (ex valueupdate)
			this.estimate.initializeNode(h, rollout2);

			// return rollout
			return rollout2;
		}

		// get action selected according to UCB
		A selectedAction = this.getUCB(h);

		// sample o and s' from (s,a)
		S SPrime = this.pb.transition(state, selectedAction).sample();

		Distribution<O> observation = this.pb.observation(state, selectedAction, SPrime);
		O obs = observation.sample();

		// if belief node associated to (a,o) does not exist
		if (!h.exists(selectedAction, obs)) {

			// build node s' and initialize action children
			NodeBeliefPOMCP<S, A, O> nb = new NodeBeliefPOMCP<>();

			// set nVisit to -1 (for simulating haz not in T)
			nb.setVisit(-1);

			// add node to tree
			h.addNode(selectedAction, obs, nb);

		}

		///////////////////////////////////////////////////////
		// here is the change regarding POMCP in order to estimate beliefs
		/////////////////////////////////////////////////////

		// use bag to build a new bag (with adding of simulated State)
		Bag<S> betaPrime = this.generatorB.generate(h, beta, state, selectedAction, obs, SPrime);

		// add newBelief to child nod
		NodeBeliefPOMCP<S, A, O> node_haz = h.get(selectedAction, obs);
		Bag<S> bag_haz = node_haz.getBag();

		// for each new generated state, add its weight to result nodeBag
		for (S s : betaPrime.keySet()) {
			bag_haz.addToBag(s, betaPrime.get(s));
		}

		// build beliefs ==> for reward from current node
		Bag<S> bag_h = h.getBag();
		Belief<S> prevBelief = new Belief<>(bag_h);
		Belief<S> finalBelief = new Belief<>(bag_haz);

		// estimate reward
		double reward = this.pb.reward(prevBelief, selectedAction, finalBelief);

		// recursive simulate
		double simulate = this.simulate(SPrime, betaPrime, node_haz, depth + 1);

		// update node values and visit
		h.addVisit();
		h.addVisit(selectedAction);

		// perform update and return specific statistics
		return estimate.update(h, selectedAction, obs, reward, gamma, simulate);
	}

	private double rollout(Bag<S> bag, int depth) {
		// different from POMCP since rollout depends on estimated belief
		return this.rollout.rollout(bag, depth, this.pb);
	}

	/**
	 * @return best estimated action after search procedure (from root)
	 */
	public A getBestAction() {
		A actionMax = null;
		double valueMAx = -Double.MAX_VALUE;

		for (A a : this.pb.allAction()) {
			NodeActionPOMCP<S, A, O> actionNode = this.root.actionChildren.get(a);
			double actionValue = actionNode.getValue();
			if (PRINT_VALUES)
				// System.out.println(a + " : " + actionValue + " vis: " +
				// actionNode.getNbVisits());
				System.out.format("%6s : %+.6f vis: %6d\n", a, actionValue, actionNode.getNbVisits());
			if (actionValue > valueMAx) {
				valueMAx = actionValue;
				actionMax = a;
			}
		}
		return actionMax;
	}

	/**
	 * action selection via UCB (duplicate from POMCP)
	 * 
	 * @param h beliefnode from which select action
	 * @return selected action
	 */
	private A getUCB(NodeBeliefPOMCP<S, A, O> h) {

		// XXX duplicate from POMCP (refactoring ??)
		A actionMax = null;
		double valueMax = -Double.MAX_VALUE;

		// # visits of node N
		int N_h = h.getVisit();

		// get action that maximizes ucb
		for (A b : this.pb.allAction()) {
			// get value and visit
			double V_hb = h.actionChildren.get(b).getValue();
			double N_hb = h.actionChildren.get(b).getNbVisits();

			// if not visited, return action
			if (N_hb == 0)
				return b;

			// ucb
			double ucb = V_hb + UCB_C * Math.sqrt(Math.log(N_h) / N_hb);
			if (ucb > valueMax) {
				valueMax = ucb;
				actionMax = b;
			}
		}
		return actionMax;
	}

	/**
	 * execute an action
	 * 
	 * @param action
	 * @param observation
	 * @param subtreeKept
	 */
	public void modifyBeliefExecution(A action, O obs, boolean subtreeKept) {
		// update belief
		NodeBeliefPOMCP<S, A, O> selectedNode = this.root.get(action, obs);

		// generate belief
		// FIXME sometimes, not enough time to have visited the selected branch (time =
		// 100 and myopic)
		if (selectedNode == null) {
			throw new Error("Node has never been visited - unknown Observation");
		}
		Bag<S> newBag = selectedNode.getBag();
		while (newBag.isEmpty()) {
			// generate newBag
			System.out.println("WARNING, this transition has never been visited - need to generate new bag");
			throw new Error("end");
			/*
			 * Bag<S> bag = this.root.getBag(); Belief<S> belief = new Belief<S>(bag); S
			 * previous = belief.sample();
			 * 
			 * newBag = this.generatorB.generate(bag, previous, action, obs, null);
			 */

			// need to sample transition
			// reality does not correspond to executed simulations
			// throw new Error("WARNING : this transition has never been visited");
		}
		Belief<S> nouveauBelief = new Belief<S>(newBag);
		this.initialBelief = nouveauBelief;

		// reinitialize root
		if (subtreeKept) {
			this.root = selectedNode;
		} else {
			this.root = new NodeBeliefPOMCP<>();
			this.root.initialize(pb.allAction());

			// initialise belief root
			Bag<S> b0 = this.root.getBag();
			for (S s : this.initialBelief.keySet())
				b0.addToBag(s, this.initialBelief.get(s));
		}
	}

}
