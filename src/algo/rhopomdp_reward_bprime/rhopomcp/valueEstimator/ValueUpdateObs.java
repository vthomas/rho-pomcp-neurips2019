package algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator;

import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;

public class ValueUpdateObs<S, A, O> implements ValueEstimate<S, A, O> {


	@Override
	public void initializeNode(NodeBeliefPOMCP<S, A, O> node, double rollout) {
		// initialize node with rollout
		node.setVisit(1);
		// save rollout and Vh
		node.complement = new RollAndValueAndReward(rollout, rollout);
	}

	@Override
	public double update(NodeBeliefPOMCP<S, A, O> h, A action, O obs, double reward, double gamma, double simulate) {

		// a. compute V(ha) = 1/N(ha) Sum N(haz) (r + gamma V(haz)) for current action
		NodeActionPOMCP<S, A, O> ha = h.actionChildren.get(action);

		// a.1 store last reward associated to obs
		NodeBeliefPOMCP<S, A, O> ha_obs = ha.getBeliefChildren().get(obs);
		((RollAndValueAndReward) ha_obs.complement).lastReward = reward;

		// a.2. sum N(haz) * (rho + gamma . V(haz))
		double sum = 0;
		for (O z : ha.getBeliefChildren().keySet()) {
			// FIXME: possible optimization (just on new observation by storing previous
			// reward values)
			NodeBeliefPOMCP<S, A, O> haz = ha.getBeliefChildren().get(z);
			int N_haz = haz.getVisit();
			double V_haz = ((RollAndValueAndReward) haz.complement).value;
			double rho_haz = ((RollAndValueAndReward) haz.complement).lastReward;
			sum = sum + N_haz * (rho_haz + gamma * V_haz);
		}

		// a.3. divide by N(ha)
		double V_ha_new = sum / ha.getNbVisits();

		// a.4. store V(ha)
		ha.setValue(V_ha_new);

		// b. compute V(h)

		// b.1. sum of N(ha).V(ha)
		sum = 0;
		for (A b : h.actionChildren.keySet()) {
			NodeActionPOMCP<S, A, O> hb = h.actionChildren.get(b);
			int N_hb = hb.getNbVisits();
			double V_hb = hb.getValue();
			sum = sum + N_hb * V_hb;
		}

		// b.2. add rollout
		sum = sum + ((RollAndValueAndReward) h.complement).roll;

		// b.3 divide by N(h)
		int N_h = h.getVisit();
		double V_h = sum / N_h;

		// store V(h) in rollAndValue
		((RollAndValueAndReward) h.complement).value = V_h;

		// return V(h)
		return V_h;
	}

}
