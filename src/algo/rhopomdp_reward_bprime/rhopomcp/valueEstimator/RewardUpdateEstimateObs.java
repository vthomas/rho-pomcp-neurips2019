package algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator;

import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;

/**
 * estimate new actionNode value of rho-POMCP by re-stimating the reward based
 * on new estimated belief
 * <p>
 * it is based on
 * <li>old estimated actionValue
 * <li>result of simulate call
 * <li>old estimated reward (estimated from previous belief)
 * <li>new estimated reward (estimated from updated belief)
 * <li>number of visits
 * <p>
 * in ActionNode, complement corresponds to previous reward
 * 
 *
 */

public class RewardUpdateEstimateObs<S, A, O> implements ValueEstimate<S, A, O> {

	/**
	 * @param obs useless (since averaged on all Reward from following trajectories)
	 */
	public double estimateValue(NodeBeliefPOMCP<S, A, O> node, A action, O obs, double newRho, double gamma,
			double simulate) {
		// get haz node
		NodeActionPOMCP<S, A, O> nodeAction = node.actionChildren.get(action);
		NodeBeliefPOMCP<S, A, O> nodehaz = nodeAction.get(obs);

		// get information from action node
		int N_ha = nodeAction.getNbVisits();
		double V_ha_old = nodeAction.getValue();

		// get info from obs node
		int N_haz = nodehaz.getVisit();

		// get old reward
		double oldRho = 0;
		// if first visit
		if (N_haz == 0) {
			N_haz=1;
			nodehaz.complement = (Double) newRho;
		} else if (N_haz == 1) {
			// no oldRho
			// store oldRho
			nodehaz.complement = (Double) newRho;
		} else {
			// get oldRho
			oldRho = (Double) (nodehaz.complement);
			// store oldRho
			nodehaz.complement = (Double) newRho;
		}

		double V = ((N_ha - 1.0) / N_ha) * (V_ha_old) + 1.0 / N_ha * (newRho + gamma * simulate)
				+ ((N_haz - 1.0) / N_haz) * (newRho - oldRho);
		return V;
	}

	public double returnValue(NodeBeliefPOMCP<S, A, O> nodeAction, double reward, double gamma, double simulate) {
		double R = reward + gamma * simulate;
		return R;
	}
	
	@Override
	public double update(NodeBeliefPOMCP<S, A, O> h, A selectedAction, O obs, double reward, double gamma, double simulate) {

		// compute new estimated value V(ha)
		double V_ha = this.estimateValue(h, selectedAction, obs, reward, gamma, simulate);

		// associated new value to action => V(ha)
		h.setNewValue(selectedAction, V_ha);

		// return value to parent node
		return this.returnValue(h, reward, gamma, simulate);

		
	}

	@Override
	public void initializeNode(NodeBeliefPOMCP<S, A, O> node, double rollout) {
		// nothing here
	}

}
