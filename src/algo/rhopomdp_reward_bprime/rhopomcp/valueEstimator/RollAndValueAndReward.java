package algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator;

/**
 * to store rollout and Value on beliefNode complement
 *
 */
public class RollAndValueAndReward {

	public double roll;
	public double value;
	public double lastReward;

	public RollAndValueAndReward(double r, double v) {
		this.roll = r;
		this.value = v;
		this.lastReward = 0;
	}

	public String toString() {
		return "roll: " + ((int) (100 * this.roll)) / 100. + " v:" + ((int) (this.value * 100)) / 100.;
	}

}
