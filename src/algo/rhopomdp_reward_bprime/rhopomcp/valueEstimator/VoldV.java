package algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator;

/**
 * used to store V and Vold for ValuePOMDP inc
 *
 */
public class VoldV {

	double V;
	double Vold;

	/**
	 * build storage for v and Vold
	 * 
	 * @param v value
	 */
	public VoldV(double v) {
		this.Vold = 0;
		this.V = v;
	}

	/**
	 * update value and store Vold
	 * 
	 * @param v new value
	 */
	public void updateV(double v) {
		this.Vold = V;
		this.V = v;
	}

	public String toString() {
		return "V:" + ((int)(100*this.V)/100.) + " Vold:" + ((int)(100*this.Vold)/100.);
	}

}
