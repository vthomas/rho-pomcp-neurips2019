package algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator;

import algo.pomdp.pomcp.NodeActionPOMCP;
import algo.pomdp.pomcp.NodeBeliefPOMCP;

/**
 * implements the way POMCP estimates an action value depending on
 * <li>oldValue
 * <li>cumulated reward
 * <li>and nb of visits
 *
 */
public class POMCPEstimate<S, A, O> implements ValueEstimate<S, A, O> {

	@Override
	public void initializeNode(NodeBeliefPOMCP<S, A, O> node, double rollout) {
		// nothing here
	}

	@Override
	public double update(NodeBeliefPOMCP<S, A, O> h, A action, O obs, double reward, double gamma,
			double simulate) {
		// get nodeAction
		NodeActionPOMCP<S, A, O> ha = h.actionChildren.get(action);

		// compute updated action value V_ha according to POMCP
		double R = reward + gamma * simulate;
		double V_ha = ha.getValue();
		int N_ha = ha.getNbVisits();
		V_ha = V_ha + (R - V_ha) / N_ha;

		// associated new value to action => V(ha)
		h.setNewValue(action, V_ha);

		// return value to parent node
		return R;
	}

}
