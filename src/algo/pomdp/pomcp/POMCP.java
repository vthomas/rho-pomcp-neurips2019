package algo.pomdp.pomcp;

import algo.pomdp.pomcp.rollout.Rollout;
import model.pomdp.Belief;
import model.pomdp.POMDP;

/**
 * POMCP algorithm as presented in "Monte Carlo Planning in Large POMDPs" Silver
 * and Veness
 * 
 * <ul>
 * Parameters
 * <li>Gamma : gamma factor
 * <li>Epsilon : threshold to stop recursion
 * <li>C : UCB exploration constant
 * </ul>
 * 
 * @author vthomas
 * 
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */

public class POMCP<S, A, O> {

	private static final boolean SHOW_LOG = false;

	/**
	 * epsilon factor
	 */
	public static double EPSILON = 0.01;

	/**
	 * UCB exploration constant
	 */
	public double ucbCst;
	public double gamma;

	/**
	 * rollout policy
	 */
	public Rollout<S, A, O> rollout = null;

	/**
	 * problem to solve
	 */
	POMDP<S, A, O> pb;

	/**
	 * initial belief
	 */
	public Belief<S> initialB;

	/**
	 * belief tree
	 */
	public NodeBeliefPOMCP<S, A, O> root;

	/**
	 * true state of the system
	 */
	public S trueState;

	/**
	 * create POMCP algorithm
	 * 
	 * @param ucb ucb constant
	 */
	public POMCP(POMDP<S, A, O> probleme, Belief<S> b, Rollout<S, A, O> roll, double ucb) {
		this.pb = probleme;
		this.rollout = roll;
		this.ucbCst = ucb;
		this.gamma = probleme.getGamma();

		// initialize root node
		this.root = new NodeBeliefPOMCP<S, A, O>();
		this.root.initialize(pb.allAction());

		// initialize belief and trueState
		this.initialB = b;
		this.trueState = b.sample();

		// initialise belief root
		// FIXED after discussion with Khansa
		Bag<S> b0 = this.root.getBag();
		for (S s : this.initialB.keySet())
			b0.addToBag(s, this.initialB.get(s));
	}

	/**
	 * search method to build tree and find best action
	 * 
	 * @param nb_it number of iterations
	 * @return best estimated action
	 */
	public A search(int nb_it) {
		for (int i = 0; i < nb_it; i++) {

			// sampling state from belief
			S initialState = this.initialB.sample();
			// simulate from initial state
			this.simulate(initialState, this.root, 0);
		}
		// return best action
		A bestAction = this.getBestAction();
		return bestAction;
	}

	/**
	 * make recursive simulation from initialState
	 * 
	 * @param state current state from which the process is simulated
	 * @param node  corresponding belief node
	 * @param depth current depth in the belief tree
	 */
	private double simulate(S state, NodeBeliefPOMCP<S, A, O> node, int depth) {
		// if enough depth, stop recursion
		if (Math.pow(gamma, depth) < EPSILON) {
			System.out.println("fin du parcours");
			// return
			return 0;
		}

		// if h not in T (POMCP) ==> translated as if nbVisit = -1
		if (node.getVisit() == -1) {
			// visit to 0
			node.addVisit();

			// build actionNode children
			node.initialize(pb.allAction());

			// return rollout
			return (this.rollout(state, depth));
		}

		// get action selected according to UCB
		A selectedAction = this.getUCB(node);

		// sample o and s' from (s,a)
		S SPrime = this.pb.transition(state, selectedAction).sample();
		O obs = this.pb.observe(selectedAction, SPrime).sample();

		// if belief node associated to (a,o) does not exist
		if (!node.exists(selectedAction, obs)) {
			// build node s' and initialize action children
			NodeBeliefPOMCP<S, A, O> nb = new NodeBeliefPOMCP<>();

			// set nVisit to -1 (for simulating haz not in T)
			nb.setVisit(-1);

			// add node to tree
			node.addNode(selectedAction, obs, nb);
		}

		// recursive simulate
		double reward = this.pb.r(state, selectedAction);
		NodeBeliefPOMCP<S, A, O> beliefChild = node.get(selectedAction, obs);
		double R = reward + gamma * this.simulate(SPrime, beliefChild, depth + 1);

		// update belief and node values
		node.addBelief(state);
		node.addVisit();
		node.addVisit(selectedAction);

		// compute new estimated value V(ha)
		NodeActionPOMCP<S, A, O> nodeAction = node.actionChildren.get(selectedAction);
		double oldValue = nodeAction.getValue();
		int nbVisits = nodeAction.getNbVisits();
		double V = oldValue + (R - oldValue) / nbVisits;

		// associated new value to action => V(ha)
		node.setNewValue(selectedAction, V);

		// return R
		return R;
	}

	/**
	 * call rollout for rollout value
	 * 
	 * @param state starting state
	 * @param depth current depth
	 * @return rollout value
	 */
	private double rollout(S state, int depth) {
		return this.rollout.rollout(state, depth, pb);
	}

	/**
	 * action selection via UCB
	 * 
	 * @param node beliefnode from which select action
	 * @return selected action
	 */
	private A getUCB(NodeBeliefPOMCP<S, A, O> node) {
		A actionMax = null;
		double valueMAx = -Double.MAX_VALUE;

		// get action that maximizes ucb
		for (A action : this.pb.allAction()) {
			// get value and visit
			double actionValue = node.actionChildren.get(action).getValue();
			double visits = node.actionChildren.get(action).getNbVisits();

			// if not visited, return action
			if (visits == 0)
				return action;

			// ucb
			double ucb = actionValue + ucbCst * Math.sqrt(Math.log(node.getVisit()) / visits);
			if (ucb > valueMAx) {
				valueMAx = ucb;
				actionMax = action;
			}
		}
		return actionMax;
	}

	/**
	 * @return best estimated action after search procedure (from root)
	 */
	private A getBestAction() {
		A actionMax = null;
		double valueMAx = -Double.MAX_VALUE;

		for (A a : this.pb.allAction()) {
			double actionValue = this.root.actionChildren.get(a).getValue();
			if (SHOW_LOG)
				System.out.println(a + " : " + actionValue);
			if (actionValue > valueMAx) {
				valueMAx = actionValue;
				actionMax = a;
			}
		}
		return actionMax;
	}

	/**
	 * execute action and modify state and belief
	 * 
	 * @param action action to perform
	 * @param keep   decide to keep subtree (true) or not (false)
	 * @return reward
	 */
	public double executeAction(A action, boolean keep) {
		// execute A and get true state
		double reward = this.pb.r(this.trueState, action);
		this.trueState = this.pb.transition(this.trueState, action).sample();

		// get O
		O obs = this.pb.observe(action, this.trueState).sample();
		System.out.println(" -> " + obs);

		// modify belief state due to performed action
		modifyBeliefExecution(action, obs, keep);

		return reward;
	}

	/**
	 * modify tree due to execution of action action and having received observation
	 * obs
	 * 
	 * @param action action executed
	 * @param obs    received observation
	 * @param keep   boolean to determine if subtree is kept
	 */
	public void modifyBeliefExecution(A action, O obs, boolean keep) {
		// update belief
		NodeBeliefPOMCP<S, A, O> selectedNode = this.root.get(action, obs);

		// generate belief
		Belief<S> nouveauBelief = new Belief<S>(selectedNode.getBag());
		this.initialB = nouveauBelief;

		// reinitialize root
		if (keep) {
			this.root = selectedNode;
		} else {
			this.root = new NodeBeliefPOMCP<>();
			this.root.initialize(pb.allAction());
		}
	}

	/**
	 * execute n actions and modify state and belief
	 * 
	 * @param keep keep the subtree (keep=true) or new root (keep=false);
	 * @return discounted reward
	 */
	public double Behave(int nbActions, int nbPerAction, boolean keep) {
		// cumulated reward
		double rewardCumul = 0;

		for (int i = 0; i < nbActions; i++) {
			if (SHOW_LOG) {
				System.out.println("****" + i + "****");
				System.out.println("- true State : " + this.trueState);
				System.out.println("- belief : " + this.initialB + "\n");
			}

			// launch pomcp
			A search = this.search(nbPerAction);
			if (SHOW_LOG)
				System.out.println("\n- action: " + search);

			// execute action
			double reward = this.executeAction(search, keep);
			rewardCumul += reward * Math.pow(gamma, i);
			if (SHOW_LOG) {
				System.out.println(" -> r : " + reward);
				System.out.println(" -> " + this.initialB);
			}
		}

		return rewardCumul;
	}

}
