package algo.pomdp.pomcp;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * belief node for POMCP algorithm
 * 
 * @author vthomas
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class NodeBeliefPOMCP<S, A, O> {

	/**
	 * visit number
	 */
	private int visit;

	/**
	 * complementary information
	 */
	public Object complement;

	/**
	 * bag associated to this node (estimated belief)
	 */
	private Bag<S> statesBag;

	/**
	 * children action node
	 */
	public Map<A, NodeActionPOMCP<S, A, O>> actionChildren;

	/**
	 * create a nodebelief with initially empty belief
	 */
	public NodeBeliefPOMCP() {
		this.setVisit(0);
		// empty belief
		this.setBelief(new Bag<S>());
		// empty children list
		this.actionChildren = new HashMap<>();
	}

	/**
	 * detect if history (hao) exists where h is current node
	 * 
	 * @param action next action
	 * @param obs    next observation
	 * @return true if belief node exists
	 */
	public boolean exists(A action, O obs) {
		// get action child
		if (!this.actionChildren.containsKey(action))
			// impossible
			throw new Error("Action should have been created");
		NodeActionPOMCP<S, A, O> actionChild = this.actionChildren.get(action);

		// get observation obs
		NodeBeliefPOMCP<S, A, O> beliefChild = actionChild.get(obs);
		return (beliefChild != null);
	}

	/**
	 * initialize beliefnode by adding all empty action children
	 * 
	 * @param pb POMDP problem
	 */
	public void initialize(Collection<A> allActions) {
		// for each action
		for (A action : allActions) {
			// create action child with nbVisit=0 and value=0
			this.actionChildren.put(action, new NodeActionPOMCP<S, A, O>(action));
		}
	}

	/**
	 * add new belief node under (action,observation)
	 * 
	 * @param action action
	 * @param obs    observation
	 * @param nb     new node belief
	 */
	public void addNode(A action, O obs, NodeBeliefPOMCP<S, A, O> nb) {
		NodeActionPOMCP<S, A, O> na = this.actionChildren.get(action);
		na.getBeliefChildren().put(obs, nb);
	}

	/**
	 * get belief child from (a,o)
	 * 
	 * @param action action
	 * @param obs    observation
	 * @return belief child
	 */
	public NodeBeliefPOMCP<S, A, O> get(A action, O obs) {
		NodeActionPOMCP<S, A, O> actionChild = this.actionChildren.get(action);
		NodeBeliefPOMCP<S, A, O> beliefChild = actionChild.get(obs);
		return beliefChild;
	}

	/**
	 * add a state to the current estimated belief
	 * 
	 * @param state state to add
	 */
	public void addBelief(S state) {
		this.getBag().addToBag(state, 1);
	}

	/**
	 * add a visit to the node
	 */
	public void addVisit() {
		this.setVisit(this.getVisit() + 1);
	}

	/**
	 * add a visit to child action node
	 * 
	 * @param action selected action
	 */
	public void addVisit(A action) {
		this.actionChildren.get(action).setNbVisits(this.actionChildren.get(action).getNbVisits() + 1);
	}

	/**
	 * change value of child action node
	 * 
	 * @param action selected action
	 * @param V_ha      new value
	 */
	public void setNewValue(A action, double V_ha) {
		NodeActionPOMCP<S, A, O> ha = this.actionChildren.get(action);
		ha.setValue(V_ha);
	}

	public int getVisit() {
		return visit;
	}

	public void setVisit(int visit) {
		this.visit = visit;
	}

	public Bag<S> getBag() {
		return statesBag;
	}

	public void setBelief(Bag<S> belief) {
		this.statesBag = belief;
	}

	/**
	 * description of the tree content (with MAXDEPTH)
	 */
	public String toString() {
		int MAXDEPTH = 3;
		String res = "";
		// call recursive decription
		res = this.toString(null, 0, MAXDEPTH);
		return res;
	}

	/**
	 * recusrive description
	 * 
	 * @param curDepth current depth
	 * @param maxDepth max depth
	 * @return description of tree at maxdepth
	 */
	String toString(O obs, int curDepth, int maxDepth) {
		// rend of recursion
		if (curDepth > maxDepth)
			return "";

		// write description
		String res = "";
		for (int i = 0; i < curDepth; i++) {
			res += "  ";
		}

		// description
		res += curDepth + " nodeB v: [" + obs + "] " + this.getVisit() + " " + this.statesBag;
		res += "\n";

		// add childrenDescription
		for (A action : this.actionChildren.keySet()) {
			res += this.actionChildren.get(action).toString(curDepth + 1, maxDepth);
		}

		return res;
	}

}
