package algo.pomdp.pomcp;

/**
 * Tool to save tree on json format (for hypertree)
 * 
 * @author vthomas
 *
 */
public class LogJson<S, A, O> {

	/**
	 * root of the tree
	 */
	NodeBeliefPOMCP<S, A, O> root;

	/**
	 * create loggeur dot
	 */
	public LogJson(NodeBeliefPOMCP<S, A, O> begin) {
		this.root = begin;
	}

	/**
	 * save the tree in dot format
	 * 
	 * @param depthMax depth of the solved tree (-1 complete tree)
	 * @param fileName name of the file
	 * @return string describing built tree
	 */
	public String toJson(int depthMax) {
		// save tree in dot format
		String r = "";
		r += "{\n";

		// get tree
		NodeBeliefPOMCP<S, A, O> root = this.root;

		// recursive call for saving tree
		r += this.saveBelief(root, depthMax, 1);

		// end of tree generating
		r += "}\n";
		return r;
	}

	/* ************** BELIEF NODE ********************* */

	/**
	 * save a beliefnode
	 * 
	 * @param node     node to save
	 * @param depthMax max depth
	 * @param curDepth current depth (used for spacing)
	 * @return description of beliefnode
	 */
	private String saveBelief(NodeBeliefPOMCP<S, A, O> node, int depthMax, int curDepth) {
		// si on a parcouru la profondeur
		if (depthMax == 0)
			return "";

		// generate json for beliefnode
		String res = "";

		// add spaces
		res = addSpaces(curDepth, res);

		// add text
		res += "\"txt\" : \"";
		res += this.setText(node);
		res += "\",\n";

		// add type
		res = addSpaces(curDepth, res);
		if (curDepth != 1)
			res += "\"type\" : \"observation\",\n";
		else
			res += "\"type\" : \"root\",\n";

		// add children structure
		res = addSpaces(curDepth, res);
		res += "\"children\" : [\n";

		// add children
		if (depthMax > 1) {
			for (A a : node.actionChildren.keySet()) {
				res = addSpaces(curDepth, res);
				res += " {\n";
				res += saveAction(node.actionChildren.get(a), depthMax - 1, curDepth + 1);
				res = addSpaces(curDepth, res);
				res += " },\n";
			}
		}

		// end children structure
		res = addSpaces(curDepth, res);
		res += "]\n";

		return res;
	}

	/**
	 * return text for belief node
	 * 
	 * @param node
	 * @return
	 */
	private String setText(NodeBeliefPOMCP<S, A, O> node) {
		String visits = "vis:" + node.getVisit();
		String belief = node.getBag().toString();
		return visits + " " + belief;
	}

	/* ************** ACTION NODE ********************* */

	/**
	 * save an action node
	 * 
	 * @param nodeActionPOMCP action node to save
	 * @param depthMax        max depth
	 * @return description of actionNode
	 */
	private String saveAction(NodeActionPOMCP<S, A, O> nodeAction, int depthMax, int curDepth) {
		// if depthmax is attained
		if (depthMax == 0)
			return "";

		// save actionNode
		String res = "";
		res = addSpaces(curDepth, res);
		res += "\"txt\" : \"";
		res += this.setText(nodeAction);
		res += "\",\n";

		// add type
		res = addSpaces(curDepth, res);
		res += "\"type\" : \"action\",\n";

		// add begin of children structure
		res = addSpaces(curDepth, res);
		res += "\"children\" : [\n";

		// add children
		if (depthMax > 1) {
			for (O obs : nodeAction.getBeliefChildren().keySet()) {
				// add observation node
				res = addSpaces(curDepth, res);
				res += "{\n";
				res = addSpaces(curDepth, res);
				res += "\"txt\" : \"" + obs + "\",\n";
				res = addSpaces(curDepth, res);
				res += "\"children\" : [\n";

				// add children
				res = addSpaces(curDepth, res);
				res += "{\n";
				res += this.saveBelief(nodeAction.getBeliefChildren().get(obs), depthMax - 1, curDepth + 1);
				res = addSpaces(curDepth, res);
				res += "},\n";
				res = addSpaces(curDepth, res);
				res += "]\n";
				res = addSpaces(curDepth, res);
				res += "},\n";

			}
		}

		// add end of children structure
		res = addSpaces(curDepth, res);
		res += "]\n";

		return res;
	}

	/**
	 * return action name
	 * 
	 * @param nodeAction
	 * @return
	 */
	private String setText(NodeActionPOMCP<S, A, O> nodeAction) {
		String name = nodeAction.action.toString();
		String val = "(vis:" + nodeAction.getNbVisits() + " val:" + nodeAction.getValue() + ")";
		return name + " " + val;
	}

	/**
	 * just to add spaces for indentation
	 * 
	 * @param curDepth current depth
	 * @param res      current string
	 * @return new string with spaces
	 */
	private String addSpaces(int curDepth, String res) {
		for (int i = 0; i < curDepth * 2; i++)
			res += " ";
		return res;
	}
}
