package algo.pomdp.pomcp;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Tool to save tree on dot format
 * 
 * @author vthomas
 *
 */
public class LogDot<S, A, O> {

	/**
	 * to give node ID
	 */
	private int numNode = 0;

	/**
	 * root of the tree
	 */
	NodeBeliefPOMCP<S, A, O> root;

	/**
	 * create loggeur dot
	 */
	public LogDot(NodeBeliefPOMCP<S, A, O> begin) {
		this.root = begin;
	}

	/**
	 * save the tree in dot format
	 * 
	 * @param depthMax depth of the solved tree (-1 complete tree)
	 * @param fileName name of the file
	 * @return string describing built tree
	 */
	public String toDot(int depthMax) {
		// save tree in dot format
		String r = "";
		r += "digraph{\n";

		// get tree
		NodeBeliefPOMCP<S, A, O> root = this.root;

		// recursive call for saving tree
		r += this.saveBelief(root, depthMax);

		// end of tree generating
		r += "}\n";
		return r;
	}

	/**
	 * save a beliefnode
	 * 
	 * @param node     node to save
	 * @param depthMax max depth
	 * @return description of beliefnode
	 */
	private String saveBelief(NodeBeliefPOMCP<S, A, O> node, int depthMax) {
		// generate dot for beliefnode
		String res = "";
		String nomState = "node" + numNode;
		String contenu = "nv:" + node.getVisit();
		// if complement
		if (node.complement != null)
			contenu += "\\n comp:" + node.complement;
		res += nomState + "[label=\"" + contenu + "\"]\n";

		// si on a parcouru la profondeur
		if (depthMax == 0)
			return res;

		// pour chaque noeud action
		for (A a : node.actionChildren.keySet()) {
			numNode++;
			res += nomState + " -> " + "nodeA" + numNode + "\n";
			res += saveAction(node.actionChildren.get(a), depthMax - 1);
		}

		return res;
	}

	/**
	 * save a action node
	 * 
	 * @param nodeActionPOMCP action node to save
	 * @param depthMax        max depth
	 * @return description of actionNode
	 */
	private String saveAction(NodeActionPOMCP<S, A, O> nodeAction, int depthMax) {
		// save actionNode
		String res = "";
		String nomAction = "nodeA" + numNode;
		double valeur = nodeAction.getValue();
		NumberFormat fm = new DecimalFormat("#0.00");
		String contenu = nodeAction.action + " - nv:" + nodeAction.getNbVisits() + "\\n val:" + fm.format(valeur);
		// if complement
		if (nodeAction.complement != null)
			contenu += "\\n comp:" + nodeAction.complement;
		res = nomAction + "[label=\"" + contenu + "\" color=\"red\" shape=\"box\"]\n";

		// si on a parcouru la profondeur
		if (depthMax == 0)
			return res;

		// on dessine les noeuds fils
		for (O obs : nodeAction.getBeliefChildren().keySet()) {
			numNode++;
			res += nomAction + " -> " + "node" + numNode + "[label=\"" + obs + "\"]\n";
			res += this.saveBelief(nodeAction.getBeliefChildren().get(obs), depthMax - 1);
		}

		return res;
	}

}
