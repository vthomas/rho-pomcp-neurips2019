package algo.pomdp.pomcp;

import java.util.HashMap;
import java.util.Map;

/**
 * Action node for POMCP algorithm
 * 
 * @author vthomas
 *
 * @param <S> State space
 * @param <A> Action space
 * @param <O> Observation space
 */
public class NodeActionPOMCP<S, A, O> {

	/**
	 * belief children (indexed by observation)
	 */
	private Map<O, NodeBeliefPOMCP<S, A, O>> beliefChildren;

	/**
	 * values
	 */
	private int nbVisits;
	private double value;
	A action;

	/**
	 * complementary information (for memorization in rhoPOMCP value estimate)
	 */
	public Object complement;

	/**
	 * creation of actionNode with 0 visit, 0 value
	 * 
	 * @param action corresponding action
	 */
	public NodeActionPOMCP(A action) {
		this.setNbVisits(0);
		this.setValue(0);
		this.action = action;
		this.beliefChildren = new HashMap<>();
	}

	/**
	 * get Observation child
	 * 
	 * @param obs observation
	 * @return nodeBelief corresponding to obs observation
	 */
	public NodeBeliefPOMCP<S, A, O> get(O obs) {
		return beliefChildren.get(obs);
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getNbVisits() {
		return nbVisits;
	}

	public void setNbVisits(int nbVisits) {
		this.nbVisits = nbVisits;
	}

	public Map<O, NodeBeliefPOMCP<S, A, O>> getBeliefChildren() {
		return beliefChildren;
	}

	/**
	 * write actionNode description
	 * 
	 * @param curDepth current depth
	 * @param maxDepth max depth
	 * @return actionNode description
	 */
	public String toString(int curDepth, int maxDepth) {
		// rend of recursion
		if (curDepth > maxDepth)
			return "";

		// write description
		String res = "";
		for (int i = 0; i < curDepth; i++) {
			res += "  ";
		}

		res += curDepth + " nAction:" + this.action + "\t nv: " + this.nbVisits + " V(ha): " + this.value;
		res += "\n";

		// add children
		for (O obs : this.beliefChildren.keySet()) {
			res += this.beliefChildren.get(obs).toString(obs,curDepth + 1, maxDepth);
		}

		return res;
	}

}
