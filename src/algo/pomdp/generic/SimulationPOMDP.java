package algo.pomdp.generic;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import model.pomdp.Belief;
import model.pomdp.POMDP;

/**
 * generic class to assess POMDP algorithm
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class SimulationPOMDP<S, A, O> {

	public static boolean LOG = false;

	/**
	 * policy POMDP (encapsulate algorithm and how it evolves after execution)
	 */
	AlgoPOMDP<S, A, O> policy;

	/**
	 * probleme pomdp
	 */
	POMDP<S, A, O> problem;

	/**
	 * create a simulator with a POMDP Policy
	 */
	public SimulationPOMDP(AlgoPOMDP<S, A, O> policy, POMDP<S, A, O> pb) {
		// TODO do not duplicate pb (since it is also in policy)
		this.policy = policy;
		this.problem = pb;
	}

	/**
	 * launch several simulations
	 * 
	 * @param nbSimulations number of episodes
	 * @param nbAction      number of actions by episode
	 * @param b0            initial belief
	 */
	public void launchSimulations(int nbSimulations, int nbActions, Belief<S> b0) {
		// liste des performances
		List<Double> perfs = new ArrayList<>();

		// repeat episode
		for (int i = 0; i < nbSimulations; i++) {
			double res = launchOneSimulation(nbActions, b0);
			System.out.println(i + ";" + res);
			perfs.add(res);
		}

		// average
		double perfSum = 0.;
		for (Double d : perfs) {
			perfSum += d;
		}
		double average = perfSum / perfs.size();
		System.out.println("  - average : " + average);

		// variance
		double variance = 0.;
		for (Double d : perfs) {
			double dist = average - d;
			variance += dist * dist;
		}
		variance = variance / perfs.size();
		System.out.println("  - variance: " + variance);

		// standard deviation
		double deviation = Math.sqrt(variance);
		System.out.println("  - std deviation: " + deviation);

		// standard error
		double stdError = deviation / Math.sqrt(perfs.size() - 1);
		System.out.println("  - std error: " + stdError);

		// interval
		System.out.println("  - interval: [" + (average - stdError) + "," + (average + stdError) + "]");

	}

	/**
	 * launch one simulation with nbActions
	 * 
	 * @param nbActions
	 */
	private double launchOneSimulation(int nbActions, Belief<S> b0) {
		// sample trueState
		S trueState = b0.sample();

		// decimal format
		DecimalFormat dc = new DecimalFormat("0.00");

		// performance criteria
		double performance = 0;
		double gamma = this.problem.getGamma();

		// reinitialise policy
		this.policy.initialize(b0);
		if (LOG) {
			System.out.println("********** NEW SIMULATION ******");
			System.out.println(b0);
		}

		// execute episode
		for (int it = 0; it < nbActions; it++) {
			if (LOG)
				System.out.println("*** iteration: " + it);

			if (LOG)
				System.out.println("  -belief:" + this.policy.getBelief().getStringTri(dc));

			A action = this.policy.getAction();

			if (LOG)
				System.out.println("  -action:" + action);

			// simulate process
			S Sprime = this.problem.transition(trueState, action).sample();
			if (LOG)
				System.out.println("  -arrival State :" + Sprime);

			// simulate observation
			O obs = this.problem.observe(action, Sprime).sample();
			if (LOG)
				System.out.println("  -observation :" + obs);

			// receive reward
			double reward = this.problem.r(trueState, action);
			if (LOG)
				System.out.println(reward);
			performance = performance + Math.pow(gamma, it) * reward;

			// update policy
			this.policy.executeAction(action, obs);

			// update trueState
			trueState = Sprime;
		}

		return performance;
	}

}
