package tools.log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Singleton for logging everything. Everything is static and method can be
 * called anywhere.
 */
public class LogSingleton {

	/**
	 * constant corresponding different logs locations
	 */
	public static int LOG_FILE = 0;
	public static int LOG_CONSOLE = 1;
	public static int LOG_EMPTY = 2;

	/**
	 * variable to determine where to write logs
	 */
	public static int LOG = LOG_EMPTY;

	/**
	 * the log object to use
	 */
	private static Log log = null;

	/**
	 * return the static log object
	 * 
	 * @param val determining where to write logs
	 * @return the static log object (singleton)
	 * 
	 */
	public static Log getLog(int val) {
		// if log object does not exist
		if (log == null) {
			// depending on where to write logs
			switch (val) {
			case 0:
				log = new LogFile(getDate());
				break;
			case 1:
				log = new LogScreen();
				break;
			case 2:
				log = new LogEmpty();
				break;
			default:
				log = new LogFile(getDate());
			}
		}
		// return log object
		return log;
	}

	/**
	 * write log with the current log object
	 * 
	 * @param s string to write
	 */
	public static void write(String s) {
		Log log = getLog(LOG);
		log.writeLog(getDate() + " : " + s);
	}

	/**
	 * Date formatting for logs
	 * 
	 * @return formatted date
	 */
	public static String getSimpleDate() {
		String day = new SimpleDateFormat("dd", Locale.FRANCE).format(new Date());
		String month = new SimpleDateFormat("MM", Locale.FRANCE).format(new Date());
		String year = new SimpleDateFormat("yyyy", Locale.FRANCE).format(new Date());
		String hour = new SimpleDateFormat("HH", Locale.FRANCE).format(new Date());
		String minute = new SimpleDateFormat("mm", Locale.FRANCE).format(new Date());
		return day + month + year + "-" + hour + minute;
	}

	/**
	 * Date formatting for logs with seconds
	 * 
	 * @return formatted date with seconds
	 */
	public static String getDate() {
		String day = new SimpleDateFormat("dd", Locale.FRANCE).format(new Date());
		String month = new SimpleDateFormat("MM", Locale.FRANCE).format(new Date());
		String year = new SimpleDateFormat("yyyy", Locale.FRANCE).format(new Date());
		String hour = new SimpleDateFormat("HH", Locale.FRANCE).format(new Date());
		String minute = new SimpleDateFormat("mm", Locale.FRANCE).format(new Date());
		String second = new SimpleDateFormat("ss", Locale.FRANCE).format(new Date());
		return day + "_" + month + "_" + year + "-" + hour + "_" + minute + "_" + second;
	}

}
