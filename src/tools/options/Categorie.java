package tools.options;

import java.util.ArrayList;
import java.util.List;

/**
 * represente une categorie d'options
 * <p>
 * permet de grouper les options ensembles
 * 
 * @author vincent.thomas@loria.fr
 *
 */
public class Categorie {

	/**
	 * le nom de la categorie
	 */
	public String nom;

	/**
	 * les noms des options concernees
	 */
	public List<String> nomOptions;

	/**
	 * la categorie
	 * 
	 * @param nom
	 *            le nom de la categorie
	 */
	public Categorie(String nom) {
		this.nomOptions = new ArrayList<>();
		this.nom = nom;
	}

	/**
	 * permet d'ajouter une option a une categorie
	 *
	 */
	public void ajouterOption(String nom) {
		this.nomOptions.add(nom);
	}

	/**
	 * affiche une categorie
	 * 
	 */
	public String toString() {
		String res = "- " + this.nom + "\n";

		// si pas d'options
		String options = "";
		for (String n : this.nomOptions) {
			options += "\t - " + n + "\n";
		}
		
		if (options.equals(""))
			return "";

		res+=options;
		return res;
	}

}
