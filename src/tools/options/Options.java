package tools.options;

import java.util.Map;
import java.util.TreeMap;

import tools.log.LogSingleton;

/**
 * represents available options.
 * <p>
 * use singleton pattern
 * 
 * @author vincent.Thomas@loria.fr
 * 
 */
public class Options {

	// //////////////////////////////// singleton

	/**
	 * static object singleton
	 */
	private static Options OPTIONS = null;

	/**
	 * static method to acces option object
	 */
	public static Options getOptions() {
		if (OPTIONS == null)
			OPTIONS = new Options();
		return (OPTIONS);
	}

	// //////////////////////////////////

	/**
	 * known option values
	 */
	Map<String, Option> options;

	/**
	 * options are listed by categories
	 */
	Categories categories;

	/**
	 * build option
	 */
	private Options() {
		this.options = new TreeMap<>();
		this.categories = new Categories();
	}

	////// ADD NEW OPTION //////////////////////

	/**
	 * add an integer value option
	 * 
	 * @param name        option name
	 * @param value       option value
	 * @param description option description
	 */
	public void setOptions(String name, int value, String descriptif) {
		if (this.options.containsKey(name))
			throw new Error("option already exists");
		this.options.put(name, new OptionEntier(name, value, descriptif));
		LogSingleton.write("initialise valeur <" + name + " -> " + value + ">");

		// add to default caterogy
		this.categories.ajouterOption(name, Categories.GENERALE);
	}

	/**
	 * add an integer value option while giving category
	 * 
	 * @param category    option category
	 * @param name        option name
	 * @param value       option value
	 * @param description option description
	 */
	public void setOptions(String category, String name, int value, String description) {
		if (this.options.containsKey(name))
			throw new Error("options existante");
		this.options.put(name, new OptionEntier(name, value, description));

		// add option to specified category
		this.categories.ajouterOption(name, category);

		String categ = this.categories.getCategoryFromOption(name);
		LogSingleton.write("initialize value <" + categ + ", " + name + " -> " + value + ">");

	}

	/**
	 * add option for string option
	 * 
	 * @param category    option category
	 * @param name        option name
	 * @param value       option value
	 * @param description option description
	 */
	public void setOptions(String category, String name, String value, String description) {
		if (this.options.containsKey(name))
			throw new Error("option already exists");
		this.options.put(name, new OptionString(name, value, description));

		// add option to specified category
		this.categories.ajouterOption(name, category);

		String categ = this.categories.getCategoryFromOption(name);
		LogSingleton.write("initialize value <" + categ + ", " + name + " -> " + value + ">");
	}

	/**
	 * add double option
	 * 
	 * @param category    option category
	 * @param name        option name
	 * @param d           option value
	 * @param description option description
	 */
	public void setOptions(String category, String name, double d, String description) {
		if (this.options.containsKey(name))
			throw new Error("option already exists");
		this.options.put(name, new OptionDouble(name, d, description));

		// add option to specified category
		this.categories.ajouterOption(name, category);

		String categ = this.categories.getCategoryFromOption(name);
		LogSingleton.write("initialize value <" + categ + ", " + name + " -> " + d + ">");
	}

	////// ADD NEW OPTION //////////////////////

	/**
	 * modify integer value of existing option
	 * 
	 * @param name   option name
	 * @param valeur option value
	 */
	public void changeOptions(String name, int valeur) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante");
		this.options.get(name).setValue(valeur);
		String categ = this.categories.getCategoryFromOption(name);
		LogSingleton.write("change value <" + categ + "," + name + " -> " + valeur + ">");
	}

	/**
	 * change l'option
	 * 
	 * @param nameState le nom de l'option
	 * @param val la valeur de l'option
	 */
	public void changeOptions(String name, String val) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante");
		String categ = this.categories.getCategoryFromOption(name);
		LogSingleton.write("change value <" + categ + "," + name + " -> " + val + ">");
		this.options.get(name).setValue(val);
	}

	/**
	 * accede a l' option
	 * 
	 * @param name nom de l'option
	 */
	public Option getOption(String name) {
		if (!this.options.containsKey(name))
			throw new Error("options existante <" + name + ">");
		return this.options.get(name);
	}

	/**
	 * retourne les categories
	 * 
	 * @return
	 */
	public String afficherCategories() {
		return "" + this.categories;
	}

	/**
	 * permet d'afficher les usages des options
	 * 
	 * @param afficheHidden si vaut true, affiche les options cachees
	 * 
	 * @return descriptif de l'analyseur
	 */
	public String getUsage(boolean afficheHidden) {
		String explication = "\n";
		explication += "Les options attendues doivent etre passees sous la forme \n";
		// explication += " '-nom' pour ajouter un flag ayant nom pour nom\n";
		explication += "   '-valeur=xx' pour donner la valeur xx a la variable valeur\n";
		explication += "\nLes options disponibles et leurs valeurs actuelles sont:\n";

		// parcourt des options existantes par categorie
		for (Categorie c : this.categories.categories) {
			if (!c.nomOptions.isEmpty()) {
				explication += "* <" + c.nom + ">\n";
				// explication
				for (String nom : c.nomOptions) {
					Option option = this.options.get(nom);
					// si elle n'est pas cachee, on ajoute son descriptif
					if (!option.hidden || afficheHidden)
						explication += "\t'-" + nom + "=" + option.showValue() + "' " + option.descriptif + " - "
								+ option.getType() + "\n";
				}
			}

		}
		return explication;
	}

	/**
	 * stop code due to an option error
	 */
	public void error(String errorMessage) {
		System.out.println("*ERROR* " + errorMessage);
		System.out.println(this.getUsage(true));
		throw new Error("*ERROR* " + errorMessage);
	}

	//////////////// ACCESS TO OPTION VALUES /////////////////////////

	/**
	 * access to integer option value
	 * 
	 * @param name option name
	 * @return integer value
	 */
	public int getValInt(String name) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante <" + name + ">");
		return this.options.get(name).getValueInt();
	}

	/**
	 * access to string option value
	 * 
	 * @param name option name
	 * @return string value
	 */
	public String getValString(String name) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante <" + name + ">");
		return this.options.get(name).getValueString();
	}

	/**
	 * access to double option value
	 * 
	 * @param name option name
	 * @return double value
	 */
	public double getValDouble(String name) {
		if (!this.options.containsKey(name))
			throw new Error("options inexistante <" + name + ">");
		return this.options.get(name).getValueDouble();
	}

}
