package tools.options;

import java.util.ArrayList;
import java.util.List;

/**
 * contient la liste des options triees par categorie
 * 
 * @author vincent.thomas
 *
 */
public class Categories {

	/**
	 * la categorie par defaut
	 */
	public static final String GENERALE = "Options generales";

	/**
	 * la liste des categories
	 */
	List<Categorie> categories;

	/**
	 * creer la liste des categories
	 */
	public Categories() {
		this.categories = new ArrayList<>();
		this.categories.add(new Categorie(GENERALE));
	}

	/**
	 * ajouter une option a une categorie
	 */
	public void ajouterOption(String nom, String nomCategorie) {
		Categorie result = null;
		// trouver la categorie
		for (int i = 0; i < this.categories.size(); i++) {
			if (this.categories.get(i).nom.equals(nomCategorie)) {
				result = this.categories.get(i);
				break;
			}
		}

		// si elle n'existe pas la creer et ajouter l'option
		if (result == null) {
			result = new Categorie(nomCategorie);
			this.categories.add(result);
		}

		// ajouter l'option
		result.nomOptions.add(nom);
	}

	/**
	 * gives the category of a specified option
	 * 
	 * @param optionName
	 * @return name of the category containing option optionName
	 */
	public String getCategoryFromOption(String optionName) {
		// find right category
		for (Categorie categ : this.categories) {
			if (categ.nomOptions.contains(optionName))
				return categ.nom;
		}
		return "Unknown Categ";
	}

	/**
	 * affiche les categories
	 */
	public String toString() {
		String res = "";
		for (Categorie c : categories)
			res += c;
		return res;
	}

}
