package tools;

import java.util.Random;

/**
 * Tool to use random generator
 *
 */
public class RandomGen {

	/**
	 * random used
	 */
	private static Random random = CreateRandom();
	private static long currentSeed;

	/**
	 * create random using time
	 * 
	 * @return
	 */
	private static Random CreateRandom() {
		Random rgen = new Random();

		// add random to seed to prevent biais due to class Random
		// https://stackoverflow.com/questions/27760450/first-random-number-after-setseed-in-java-always-similar
		currentSeed = rgen.nextLong();

		rgen.setSeed(currentSeed);
		return rgen;
	}

	/**
	 * get float
	 */
	public static double random() {
		double nextDouble = random.nextDouble();
		return nextDouble;
	}

	/**
	 * get int
	 */
	public static int randomInt(int bound) {
		int nextInt = random.nextInt(bound);
		return nextInt;
	}

	/**
	 * change seed
	 */
	public static void changeSeed(long seed) {
		System.out.println("seed:" + seed);
		random = new Random(seed);
	}

	/**
	 * remove seed
	 */
	public static void removeSeed() {
		System.out.println("remove seed");
		random = CreateRandom();
	}

	/**
	 * return current seed
	 */
	public static long getSeed() {
		return currentSeed;
	}

	/**
	 * test randomgen
	 */
	public static void main(String args[]) {
		System.out.println("** random **");
		for (int i = 0; i < 10; i++)
			System.out.println(RandomGen.randomInt(10));

		System.out.println("** seed 42 **");
		RandomGen.changeSeed(42);
		for (int i = 0; i < 10; i++)
			System.out.println(RandomGen.randomInt(10));
	}

	public static long newSeed() {
		// get random seed to prevent biais due to class Random
		// https://stackoverflow.com/questions/27760450/first-random-number-after-setseed-in-java-always-similar
		currentSeed = random.nextLong();
		random.setSeed(currentSeed);
		return currentSeed;
	}

}
