package model.pomdp;

import java.util.*;

import model.mdp.*;

/**
 * update a belief state
 * 
 * @param <S> States
 * @param <A> Actions
 * @param <O> Observations
 */
public class BeliefUpdate<S, A, O> {

	POMDP<S, A, O> pomdp;

	/**
	 * building beliefupdate
	 * 
	 * @param pb POMDP problem
	 */
	public BeliefUpdate(POMDP<S, A, O> pb) {
		this.pomdp = pb;
	}

	/**
	 * bayes update of a belief
	 * 
	 * @param b   initial belief
	 * @param a   performed action
	 * @param obs performed observation
	 * @return new belief
	 */
	public Belief<S> updateBelief(Belief<S> b, A a, O obs) {
		// creates resulting belief
		Belief<S> apresT = new Belief<>();

		// transition from each initial state
		for (S initialS : b.keySet()) {
			// for all arrival state
			Distribution<S> transition = this.pomdp.transition(initialS, a);
			Set<S> sPrimes = transition.getElements();
			// add contribution of arrival state from this initial state
			for (S arrivalS : sPrimes) {
				// transition probability
				double probaT = transition.getProba(arrivalS);
				// add to resulting belief
				if (!apresT.belief.containsKey(arrivalS)) {
					apresT.belief.put(arrivalS, 0.);
				}
				double oldValue = apresT.get(arrivalS);
				apresT.put(arrivalS, oldValue + probaT * b.get(initialS));
			}
		}

		// add observation probability 
		for (S s : apresT.keySet()) {
			Distribution<O> distObs = this.pomdp.observe(a, s);
			double p_obs = distObs.getProba(obs);
			apresT.put(s, apresT.get(s) * p_obs);
		}

		// normalize the distribution 
		apresT.normaliser();
		return apresT;

	}

}
