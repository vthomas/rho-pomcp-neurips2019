package model.pomdp;

import java.util.List;

import model.mdp.*;

/**
 * model of a POMDP
 * 
 * @author vthomas
 * 
 * @param <S> state class
 * @param <A> action class
 * @param <O> observation classe
 */
public interface POMDP<S, A, O> {
	/**
	 * reward function
	 * 
	 * @param initS  initial state
	 * @param action performed action
	 * @return reward associated to transition
	 */
	public abstract double r(S initS, A action);

	/**
	 * distribution over arrival state from initial S when performing action a
	 * 
	 * @param s      initial state
	 * @param action performed action
	 * @return distribution over arrival state
	 */
	public abstract Distribution<S> transition(S s, A action);

	/**
	 * @return set of possible states
	 */
	public abstract List<S> allStates();

	/**
	 * @return set of possible actions
	 */
	public abstract List<A> allAction();

	/**
	 * probability distribution over observation o given action performed and
	 * arrival state P(o|a,s')
	 * 
	 * @param action performed action
	 * @param arrivS arrival state
	 * @return distribution over observations
	 */
	public abstract Distribution<O> observe(A action, S arrivS);

	/**
	 * @return set of possible observations
	 */
	public abstract List<O> allObs();

	/**
	 * @return GAMMA coef (to be sure to use the same gamma)
	 */
	public abstract double getGamma();

}
