package model.pomdp;

import java.text.DecimalFormat;
import java.util.*;

import algo.pomdp.pomcp.Bag;
import tools.RandomGen;

/**
 * POMDP belief state
 * 
 */
public class Belief<S> {

	/**
	 * state probabilities
	 */
	Map<S, Double> belief;

	/**
	 * building empty belief state
	 */
	public Belief() {
		this.belief = new HashMap<S, Double>();
	}

	/**
	 * build empty belief state from list of states
	 */
	public Belief(List<S> states) {
		this.belief = new HashMap<S, Double>();
		for (S state : states)
			belief.put(state, 0.);
	}

	/**
	 * build a belief from particles
	 * 
	 * @param particles as Bag
	 */
	public Belief(Bag<S> particles) {
		this.belief = new HashMap<S, Double>();

		double sum = 0;
		for (S state : particles.keySet()) {
			sum += particles.get(state);
		}

		if (sum == 0)
			throw new Error("belief empty");

		for (S state : particles.keySet()) {
			double proba = (particles.get(state)) / sum;
			belief.put(state, proba);
		}

	}

	/**
	 * belief normalizing
	 */
	public void normaliser() {
		double somme = 0;
		for (S o : this.belief.keySet())
			somme += belief.get(o);
		for (S o : this.belief.keySet())
			belief.put(o, belief.get(o) / somme);
	}

	/**
	 * sample from belief
	 * 
	 * @return sampled state
	 */
	public S sample() {
		// random sampling
		double rnd = RandomGen.random();

		// use belief
		for (S o : this.belief.keySet()) {
			if (rnd <= belief.get(o))
				return o;
			else
				rnd = rnd - belief.get(o);
		}

		throw new Error("devrait pas arriver la");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((belief == null) ? 0 : belief.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Belief<S> other = (Belief<S>) obj;
		if (belief == null) {
			if (other.belief != null)
				return false;
		} else if (!belief.equals(other.belief))
			return false;
		return true;
	}

	/**
	 * distance between two beliefs
	 * 
	 * @param belief2 other belief
	 * @return distance
	 */
	public double distance(Belief<S> belief2) {
		double somme = 0;
		for (S o : this.belief.keySet()) {

			// to deal with sparse belief
			double d2 = 0;
			if (belief2.keySet().contains(o)) {
				d2 = belief2.belief.get(o);
			}

			double diff = this.belief.get(o) - d2;
			somme = somme + diff * diff;
		}
		return Math.sqrt(somme);
	}

	/**
	 * change a belief value
	 * 
	 * @param state state
	 * @param d     new value
	 */
	public void put(S state, double d) {
		this.belief.put(state, d);
	}

	/**
	 * 
	 * @param state etat
	 * @return state probability
	 */
	public double get(S state) {
		return this.belief.get(state);
	}

	public String toString() {
		return this.belief.toString();
	}

	/**
	 * set uniform distribution
	 */
	public void setUniform() {
		int nb = this.belief.keySet().size();
		for (S s : this.belief.keySet()) {
			this.belief.put(s, 1.0 / nb);
		}
	}

	/**
	 * @return max probability from belief
	 */
	public double getMaxProba() {
		double max = 0.;
		for (S s : this.belief.keySet()) {
			if (this.belief.get(s) > max)
				max = this.belief.get(s);
		}
		return max;
	}

	/**
	 * @return keys from mapping
	 */
	public Set<S> keySet() {
		return this.belief.keySet();
	}

	/**
	 * return formatted belief
	 * 
	 * @param dc decimalformatting
	 * @return descripition of belief
	 */
	public String getString(DecimalFormat dc) {
		String res = "{ ";
		for (S state : this.belief.keySet()) {
			if (this.belief.get(state) > 0)
				res += state + "=" + dc.format(this.belief.get(state)) + " ";
		}
		res += "}";
		return res;
	}

	/**
	 * return formatted belief
	 * 
	 * @param dc decimalformatting
	 * @return descripition of belief
	 */
	public String getStringTri(DecimalFormat dc) {
		// build sorted map by values
		TreeSet<S> map = new TreeSet<S>(new Comparator<S>() {
			@Override
			public int compare(S o1, S o2) {
				if (belief.get(o1) <= belief.get(o2))
					return 1;
				else
					return -1;
			}
		});

		for (S state : this.belief.keySet()) {
			map.add(state);
		}

		String res = "{ ";
		for (S state : map) {
			if (this.belief.get(state) > 0)
				res += state + "=" + dc.format(this.belief.get(state)) + " ";
		}
		res += "}";
		return res;
	}

	/**
	 * returns shannon entropy of the belief distribution
	 */
	public double getEntropy() {
		double ent = 0;
		for (S state : this.belief.keySet()) {
			double prob = this.belief.get(state);
			// to prevent nan
			if (prob != 0)
				ent -= prob * Math.log(prob);
		}
		return ent;
	}

}
