package model.mdp;


/**
 * represents a state (forces to write hashcode and equals to prevent errors)
 */
public abstract class StateAbstract {

	public abstract int hashCode();

	public abstract boolean equals(Object obj);

}
