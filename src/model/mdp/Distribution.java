package model.mdp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import tools.RandomGen;

/**
 * 
 * represents a probability distributions on elements
 * 
 * <p/>
 * implement Iterable<E> so that simplified iteration can be used on a
 * distribution.
 * 
 * <code>
 * for (E elem : distribution)
 * {
 * }
 * </code>
 * 
 */
public class Distribution<E> implements Iterable<E> {

	/**
	 * the probability distribution
	 */
	private Map<E, Double> probabilities;

	/**
	 * create an empty distribution
	 */
	public Distribution() {
		this.probabilities = new HashMap<E, Double>();
	}

	/**
	 * add a probability to a specified element
	 * 
	 * @param elem  elem to add to the distribution
	 * @param proba associated probability
	 */
	public void addProbability(E elem, double proba) {
		// if elem already contained,
		if (this.probabilities.containsKey(elem)) {
			// add probability to existing one
			double newProba = this.probabilities.get(elem) + proba;
			this.probabilities.put(elem, newProba);
		} else {
			// add probability with new element
			this.probabilities.put(elem, proba);
		}
	}

	/**
	 * replace probability for a specified element
	 * 
	 * @param elem  elem to add to the distribution
	 * @param proba associated probability
	 */
	public void replace(E elem, double proba) {
		// change probability with new element
		this.probabilities.put(elem, proba);
	}

	/**
	 * sample one element according to the distribution
	 */
	public E sample() {
		double sum = 0;
		double random = RandomGen.random();

		// iterate on elements
		for (E elem : this.probabilities.keySet()) {
			// add proba to sum
			sum = sum + this.probabilities.get(elem);

			// if random is attained
			if (random < sum) {
				return (elem);
			}
		}

		throw new Error("Problem with sampling !!");
	}

	/**
	 * @param elem element whose probability is returned
	 * @return returns probability associated to element elem
	 */
	public double getProba(E elem) {
		if (!this.probabilities.containsKey(elem))
			return 0;
		else
			return this.probabilities.get(elem);
	}

	/**
	 * 
	 * @return elements of distribution
	 */
	public Set<E> getElements() {
		return this.probabilities.keySet();
	}

	/**
	 * to make distribution iterable on the elements
	 */
	@Override
	public Iterator<E> iterator() {
		return this.probabilities.keySet().iterator();
	}

	/**
	 * @return number of elements
	 */
	public int size() {
		return this.probabilities.keySet().size();
	}

	public Set<E> keySet() {
		return this.probabilities.keySet();
	}

	public String toString() {
		String res = "{";
		for (E el : this.probabilities.keySet()) {
			res = res + ", " + el + "->" + this.probabilities.get(el);
		}
		res += "}";
		return res;
	}

}
