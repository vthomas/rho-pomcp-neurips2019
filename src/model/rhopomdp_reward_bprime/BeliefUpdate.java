package model.rhopomdp_reward_bprime;

import java.util.HashMap;
import java.util.Map;

import model.mdp.Distribution;
import model.pomdp.Belief;

/**
 * tool to update belief in rhoPOMDP
 *
 * @param <S> state space
 * @param <A> action space
 * @param <O> observation space
 */
public class BeliefUpdate<S, A, O> {

	/**
	 * rhoPOMDP problem
	 */
	RhoPOMDP<S, A, O> problem;

	/**
	 * create a beliefupdater
	 * 
	 * @param rhopb problem
	 */
	public BeliefUpdate(RhoPOMDP<S, A, O> rhopb) {
		this.problem = rhopb;
	}

	/**
	 * update belief according to action and observation
	 * 
	 * @param b      current belief
	 * @param action current action
	 * @param obs    current observation
	 * @return belief on next step
	 */
	public Belief<S> updateBelief(Belief<S> b, A action, O obs) {

		// compute probability of observation
		// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s,s'|a)
		// P(O | a) = Sum_{s,s'} P(O|s,s',a) P(s'|a,s) P(s)
		

		// for each initial state in b
		// compute contribution to pSPRime = P(O,s'|a) = Sum_s
		// p(s'|s,a)*p(o|s,s',a)*p(s)
		Map<S, Double> pS = new HashMap<>();
		for (S s : b.keySet()) {
			// get transition
			Distribution<S> transition = this.problem.transition(s, action);
			// for each reachable s'
			for (S sPrime : transition) {
				// p(s'|s,a)
				double probaT = this.problem.transition(s, action).getProba(sPrime);
				// p(o|s,s',a)
				double probaO = this.problem.observation(s, action, sPrime).getProba(obs);
				// belief s
				double bs = b.get(s);

				// add bs * pobs* ptrans = P(O,s,s'|a) to P(O,s'|a)
				double old = 0;
				if (pS.containsKey(sPrime)) {
					old = pS.get(sPrime);
				}
				double deltaProba = probaO * probaT * bs;
				pS.put(sPrime, old + deltaProba);
			}
		}

		// compute P(O | a) = Sum for each s' P(O,s'|a)
		double proba = 0;
		for (S sPrime : pS.keySet()) {
			proba += pS.get(sPrime);
		}

		// if observation is possible
		if (proba > 0) {
			// proba is P(O|a) (computed)
			// ps is P(s',o|a)
			// estimate immediate reward (myopic)

			// compute bPrime
			Belief<S> bPrime = new Belief<>();
			// P(s'|o) = P(s',o|a) / P(o|a)
			for (S sPrime : pS.keySet()) {
				bPrime.put(sPrime, pS.get(sPrime) / proba);
			}
			return bPrime;
		} else {
			// observation impossible
			throw new Error("Observation impossible " + action + ";" + obs);
		}

	}

	/**
	 * update belief according to action
	 * 
	 * @param b      current belief
	 * @param action current action
	 * @return belief on next step
	 */
	public Belief<S> updateBelief(Belief<S> b, A action) {
		// sample s from b
		S initialS = b.sample();
		// sample s' from transition
		S finalS = this.problem.transition(initialS, action).sample();
		// sample o from s'
		O obs = this.problem.observation(initialS, action, finalS).sample();

		return this.updateBelief(b, action, obs);
	}

}
