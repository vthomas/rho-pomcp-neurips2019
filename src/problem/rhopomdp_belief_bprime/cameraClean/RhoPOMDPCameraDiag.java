package problem.rhopomdp_belief_bprime.cameraClean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * camera clean problem coming from Maurico Araya Thesis (page 65) Diagnosis
 * Variant.
 * 
 * <ul>
 * <li>State = <camera,object,lens>
 * <li>Action = {move, clean, shoot}
 * <li>Obs ={true, false, nophoto}
 * </ul>
 *
 */
public class RhoPOMDPCameraDiag implements RhoPOMDP<StateCamera, ActionCamera, ObsCamera> {

	private static final double PROBA_CORRECT_DIRTY = 0.55;
	private static final double PROBA_CORRECT_CLEAN = 0.8;

	/**
	 * the number of possible zones
	 */
	int nbZones;

	/**
	 * list of states
	 */
	List<StateCamera> states;

	/**
	 * list of actions
	 */
	List<ActionCamera> actions;

	/**
	 * list of observations
	 */
	List<ObsCamera> obs;

	/**
	 * creat a camera clean pb
	 * 
	 * @param zones number of zones
	 */
	public RhoPOMDPCameraDiag(int zones) {
		this.nbZones = zones;

		// initialize state list
		this.states = new ArrayList<>();
		for (int cam = 0; cam < zones; cam++)
			for (int obj = 0; obj < zones; obj++) {
				states.add(new StateCamera(cam, obj, false));
				states.add(new StateCamera(cam, obj, true));
			}

		// initialize actions
		this.actions = Arrays.asList(ActionCamera.ACTIONS);

		// initialize observations
		this.obs = Arrays.asList(ObsCamera.ALL_OBS);
	}

	// ******************* REWARD For diagnosis ***********************

	/**
	 * reward is difference of entropy (cf fig 49 p65, not the same as what the text
	 * explains)
	 */
	@Override
	public double reward(Belief<StateCamera> dep, ActionCamera action, Belief<StateCamera> arr) {
		double negEntropyArr = -arr.getEntropy();
		double negEntropyDep = -dep.getEntropy();

		return (negEntropyArr - negEntropyDep);
	}

	// ******************* TRANSITION ***********************

	/**
	 * transition distribution on s' depending on s and action
	 * <ul>
	 * <li>camera zone is modified via 'move' action (deterministic)
	 * <li>cleanness of camera is modified via 'clean' and 'shoot' action
	 * (deterministic)
	 * <li>position of object is fixed
	 * </ul>
	 */
	@Override
	public Distribution<StateCamera> transition(StateCamera s, ActionCamera action) {

		// if action == MOVE, just change cameraZone
		if (action == ActionCamera.MOVE) {
			StateCamera clone = new StateCamera(s);
			clone.advanceCamera(this.nbZones);
			Distribution<StateCamera> d = new Distribution<>();
			d.addProbability(clone, 1);
			return d;
		}

		// if action == CLEAN, just change lensClean to true
		else if (action == ActionCamera.CLEAN) {
			StateCamera clone = new StateCamera(s);
			clone.lensClean = true;
			Distribution<StateCamera> d = new Distribution<>();
			d.addProbability(clone, 1);
			return d;
		}

		// if action == SHOOT, just change lensClean to false
		else if (action == ActionCamera.SHOOT) {
			StateCamera clone = new StateCamera(s);
			clone.lensClean = false;
			Distribution<StateCamera> d = new Distribution<>();
			d.addProbability(clone, 1);
			return d;
		}

		// action impossible
		else {
			throw new Error("ACTION NOT FOUND !!!");
		}
	}

	// ******************* OBSERVATION ***********************

	@Override
	/**
	 * observation function. Observation is stochastic and depends on the cleanness
	 * of the lens of the camera
	 * 
	 * <ul>
	 * <li>if lens is clean => proba of correct observation = 0.8
	 * <li>if lens is dirty => proba of correct observation = 0.55
	 * </ul>
	 * 
	 */
	public Distribution<ObsCamera> observation(StateCamera starting, ActionCamera action, StateCamera fin) {
		// distribution
		Distribution<ObsCamera> distrib = new Distribution<>();

		// if action is not shoot, observation is nophoto
		if (action != ActionCamera.SHOOT) {
			distrib.addProbability(ObsCamera.NOPHOTO, 1);
			return distrib;
		}

		// else action = SHOOT
		// parameters
		double probaClean = PROBA_CORRECT_CLEAN;
		double probaDirty = PROBA_CORRECT_DIRTY;

		// proba = function camera is clean
		double proba = -1;
		if (starting.lensClean) {
			proba = probaClean;
		} else {
			proba = probaDirty;
		}

		// if camera on object zone
		if (starting.cameraOnObject()) {
			distrib.addProbability(ObsCamera.TRUE, proba);
			distrib.addProbability(ObsCamera.FALSE, 1 - proba);
			return distrib;
		} else {
			// camera and object on different zone
			distrib.addProbability(ObsCamera.FALSE, proba);
			distrib.addProbability(ObsCamera.TRUE, 1 - proba);
			return distrib;
		}
	}

	// ******************* GETTER ***********************

	// getter for all states
	@Override
	public List<StateCamera> allStates() {
		return this.states;
	}

	// getter for all actions
	@Override
	public List<ActionCamera> allAction() {
		return this.actions;
	}

	// getter for all observations
	@Override
	public List<ObsCamera> allObs() {
		return this.obs;
	}

	@Override
	public double getGamma() {
		return 0.95;
	}

	@Override
	public Belief<StateCamera> getB0() {
		// initial belief
		Belief<StateCamera> initialB = new Belief<>();
		// uniform distribution only on target location
		for (int i = 0; i < nbZones; i++)
			initialB.put(new StateCamera(0, i, true), 1);
		initialB.normaliser();
		return initialB;

	}

}
