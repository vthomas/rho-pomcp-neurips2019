package problem.rhopomdp_belief_bprime.cameraClean;

import model.mdp.ActionString;

/**
 * Actions for camera clean problem
 *
 */
public class ActionCamera extends ActionString {

	/**
	 * constants used
	 */
	public static ActionCamera SHOOT = new ActionCamera("Shoot");
	public static ActionCamera MOVE = new ActionCamera("Move");
	public static ActionCamera CLEAN = new ActionCamera("Clean");
	public static ActionCamera[] ACTIONS = { SHOOT, MOVE, CLEAN };

	/**
	 * creation of an action
	 * 
	 * @param s corresponding string
	 */
	private ActionCamera(String s) {
		super(s);
	}

}
