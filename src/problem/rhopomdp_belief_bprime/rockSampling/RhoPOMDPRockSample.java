package problem.rhopomdp_belief_bprime.rockSampling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * build rock sampling problem as a rhoPOMDP
 * 
 * <p>
 * based on 'Online Planning Algorithms for POMDPs' - Ross, Pineau, Paquet,
 * Chaib-draa - JAIR 2008
 * <p>
 * based on 'Heuristic search value iteration for POMDPs.' Smith, Simmons -
 * UAI04
 *
 */
public class RhoPOMDPRockSample implements RhoPOMDP<StateRock, ActionRock, ObsRock> {

	/**
	 * influence of distance on reward
	 */
	private static final double INFLUENCE_DIST = 1.;

	/**
	 * rewards
	 */
	private static final double REWARD_BAD_SAMPLE = -100;
	public static final double REWARD_GOOD_SAMPLE = 100;
	public static final double REWARD_EXIT = 10;

	/**
	 * exit
	 */
	int exitX = 0, exitY = 0;

	/**
	 * size of grid (nxn)
	 */
	int n;

	/**
	 * number of rocks
	 */
	int k;

	/**
	 * location of rocks
	 */
	RockLocations rocks;

	/**
	 * list of observations
	 */
	List<ObsRock> obs;

	/**
	 * list of actions
	 */
	List<ActionRock> actions;

	/**
	 * list of states
	 */
	List<StateRock> states = null;

	/**
	 * 
	 * @param n    size of grid
	 * @param k    number of rocks
	 * @param seed seed used to build rocksample
	 */
	public RhoPOMDPRockSample(int n, int k, long seed) {
		// save pb parameters
		this.n = n;
		this.k = k;

		// build problem
		rocks = new RockLocations(n, k, seed);

		// initialize observations
		this.obs = Arrays.asList(ObsRock.ALL_OBS);

		// initialize actions (with rockslocation)
		ActionRock.initAllActions(rocks);
		this.actions = Arrays.asList(ActionRock.getAllActions());
	}

	/**
	 * 
	 * @param n    size of grid
	 * @param k    number of rocks
	 * @param rock initial location of rocks
	 */
	public RhoPOMDPRockSample(int n, RockLocations rock) {
		// save pb parameters
		this.n = n;
		this.k = rock.rocks.size();

		// build problem
		rocks = rock;

		// initialize observations
		this.obs = Arrays.asList(ObsRock.ALL_OBS);

		// initialize actions (with rockslocation)
		ActionRock.initAllActions(rocks);
		this.actions = Arrays.asList(ActionRock.getAllActions());
	}

	@Override
	public List<ObsRock> allObs() {
		return this.obs;
	}

	@Override
	public List<StateRock> allStates() {
		if (states == null)
			states = this.generateStates();
		return this.states;
	}

	/**
	 * generate all possible states
	 */
	public List<StateRock> generateStates() {
		// all posible states
		List<StateRock> res = new ArrayList<StateRock>();

		// build all possible rock types
		List<boolean[]> rockTypesList = buildCartesianProduct();

		// for each position (x, y), each rock type
		for (int x = 0; x < this.n; x++)
			for (int y = 0; y < this.n; y++)
				// for each type possible, add new state
				for (boolean[] typ : rockTypesList) {
					res.add(new StateRock(x, y, typ));
				}

		return res;
	}

	@Override
	public List<ActionRock> allAction() {
		return this.actions;
	}

	@Override
	public double getGamma() {
		return 0.95;
	}

	@Override
	public double reward(Belief<StateRock> dep, ActionRock action, Belief<StateRock> arr) {
		// determine x and y from dep
		StateRock first = dep.keySet().iterator().next();
		int x = first.x;
		int y = first.y;

		// if action is sample
		if (action == ActionRock.SAMPLE) {
			// get rock
			int numRock = this.rocks.getRockFromLocation(x, y);
			// if rock present on location
			if (numRock != -1) {
				double val = 0;
				// for each state from belief, get associated R
				for (StateRock sr : dep.keySet()) {
					double proba = dep.get(sr);
					// if corresponding rock type is good
					if (sr.types[numRock]) {
						// add reward with proba
						val = val + proba * REWARD_GOOD_SAMPLE;
					} else {
						// add reward with proba
						val = val + proba * REWARD_BAD_SAMPLE;
					}
				}
				return val;
			}
		}

		// if arrival state is exit and start is not exit
		// (OK because location is deterministic)
		StateRock arriv = arr.keySet().iterator().next();
		if ((arriv.x == this.exitX) && (arriv.y == this.exitY) && (first.x != this.exitX || first.y != this.exitY)) {
			// return +10 and end of
			return REWARD_EXIT;
		}

		// else no reward
		return 0;
	}

	@Override
	public Distribution<StateRock> transition(StateRock s, ActionRock action) {
		// Duplicate state
		StateRock arriv = new StateRock(s);
		// resulting distribution
		Distribution<StateRock> dist = new Distribution<>();

		// if on exit state => nothing
		if ((arriv.x == this.exitX) && (arriv.y == this.exitY)) {
			dist.addProbability(arriv, 1.0);
			return dist;
		}

		// if action is sampling
		if (action == ActionRock.SAMPLE) {
			// get rock if exists
			int numRock = this.rocks.getRockFromLocation(s.x, s.y);
			if (numRock != -1) {
				// change status of corresponding rock
				arriv.types[numRock] = false;
			}
			dist.addProbability(arriv, 1.0);
			return dist;
		}

		// ******** if action is move ************/
		if (action == ActionRock.NORTH) {
			// perform move deterministically
			arriv.y--;
			// chack grid
			if (arriv.y < 0)
				arriv.y = 0;
			dist.addProbability(arriv, 1.0);
			return dist;
		} else if (action == ActionRock.EAST) {
			// perform move deterministically
			arriv.x++;
			// check grid
			if (arriv.x > this.n - 1)
				arriv.x = this.n - 1;
			dist.addProbability(arriv, 1.0);
			return dist;
		} else if (action == ActionRock.WEST) {
			// perform move deterministically
			arriv.x--;
			// check grid
			if (arriv.x < 0)
				arriv.x = 0;
			dist.addProbability(arriv, 1.0);
			return dist;
		} else if (action == ActionRock.SOUTH) {
			// perform move deterministically
			arriv.y++;
			// check grid
			if (arriv.y > this.n - 1)
				arriv.y = this.n - 1;
			dist.addProbability(arriv, 1.0);
			return dist;
		}

		// action must be CHECK
		int numRock = action.checkedRock;
		if (numRock == -1)
			throw new Error("Pas un check");
		// no change
		dist.addProbability(arriv, 1.0);
		return dist;
	}

	@Override
	public Distribution<ObsRock> observation(StateRock starting, ActionRock action, StateRock fin) {
		Distribution<ObsRock> distObs = new Distribution<>();

		// if not check action
		if (action == ActionRock.SAMPLE || action == ActionRock.EAST || action == ActionRock.NORTH
				|| action == ActionRock.SOUTH || action == ActionRock.WEST) {
			// return NO_INFO
			distObs.addProbability(ObsRock.NONE, 1.0);
			return distObs;
		}

		// action is check
		int numRock = action.checkedRock;
		if (numRock == -1)
			throw new Error("action should have been checked");

		// get rock
		Location loc = this.rocks.getLocationFromRock(numRock);

		// compute distance between starting and loc
		int dx = loc.x - starting.x;
		int dy = loc.y - starting.y;
		double distance = Math.sqrt(dx * dx + dy * dy);

		// compute probability from distance
		double eta = Math.exp(-distance / INFLUENCE_DIST);
		double proba_correct = 0.5 + 0.5 * eta;
		// System.out.println(proba_correct);

		// get true value
		boolean type = starting.types[numRock];
		double probaGood = -1;
		if (type) {
			probaGood = proba_correct;
		} else {
			probaGood = 1 - proba_correct;
		}

		// return distribution
		distObs.addProbability(ObsRock.GOOD, probaGood);
		distObs.addProbability(ObsRock.BAD, 1 - probaGood);

		return distObs;
	}

	@Override
	public Belief<StateRock> getB0() {
		Belief<StateRock> belief = new Belief<>();
		// select initial state
		int x = this.n / 2;
		int y = this.n / 2;

		// build uniform belief on all possible rock types
		List<boolean[]> rockTypesList = buildCartesianProduct();

		// for each typepossible, add to distribution
		for (boolean[] typ : rockTypesList) {
			belief.put(new StateRock(x, y, typ), 1.0);
		}
		belief.setUniform();

		// belief initial
		return belief;
	}

	/**
	 * build list of possible rocktype
	 * 
	 * @return list of all different boolean array
	 */
	public List<boolean[]> buildCartesianProduct() {

		// create initial list
		List<boolean[]> init = new ArrayList<>();
		boolean[] a = new boolean[k];
		init.add(a);

		// loop until size k
		for (int i = 0; i < k; i++) {
			List<boolean[]> newList = new ArrayList<>();
			for (boolean[] tab : init) {
				// build two new tabs
				boolean[] tab1 = (boolean[]) (tab.clone());
				tab1[i] = true;
				newList.add(tab1);

				boolean[] tab2 = (boolean[]) (tab.clone());
				tab2[i] = false;
				newList.add(tab2);
			}
			init = newList;
		}

		return init;
	}

	/**
	 * print the problem representation
	 */
	public String drawProblem(StateRock s) {
		String res = "";
		for (int j = 0; j < this.n; j++) {
			for (int i = 0; i < this.n; i++) {

				// if exit
				if ((i == exitX) && (j == exitY))
					res += " S ";
				else {
					// get rock at this location
					Integer a = this.rocks.locations.get(new Location(i, j));

					// if rock
					if (a != null) {
						res += " " + a;
						// rock type
						if (s.types[a])
							res += "+";
						else
							res += "-";
					} else {
						res += " # ";
					}
				}
			}
			res += "\n";
		}
		return res;
	}

	/* **************************************************************** */

}
