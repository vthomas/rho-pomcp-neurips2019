package problem.rhopomdp_belief_bprime.rockSampling;

import model.pomdp.ObsString;

public class ObsRock extends ObsString {

	/**
	 * constant to define possible observations
	 */
	public static final ObsRock GOOD = new ObsRock("good");
	public static final ObsRock BAD = new ObsRock("bad");
	public static final ObsRock NONE = new ObsRock("none");
	public static final ObsRock[] ALL_OBS = { GOOD, BAD, NONE };

	/**
	 * create observation (private - only use constant action)
	 */
	private ObsRock(String s) {
		super(s);
	}

}
