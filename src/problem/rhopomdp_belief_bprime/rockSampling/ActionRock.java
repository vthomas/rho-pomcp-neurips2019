package problem.rhopomdp_belief_bprime.rockSampling;

import model.mdp.ActionString;

/**
 * Rock action
 * 
 * @author vthomas
 *
 */
public class ActionRock extends ActionString {

	/**
	 * specific constant actions
	 */
	static final ActionRock SAMPLE = new ActionRock("SAMPLE");
	static final ActionRock SOUTH = new ActionRock("SOUTH");
	static final ActionRock WEST = new ActionRock("WEST");
	static final ActionRock EAST = new ActionRock("EAST");
	static final ActionRock NORTH = new ActionRock("NORTH");
	static private ActionRock[] ALL_ACTIONS = null;

	/**
	 * num of checked rock
	 */
	int checkedRock = -1;

	/**
	 * initialize action list
	 * 
	 * @param k number of rocks;
	 */
	static public void initAllActions(RockLocations rocks) {
		// number of rocks
		int k = rocks.rocks.size();
		ALL_ACTIONS = new ActionRock[5 + k];

		// add constant actions
		ALL_ACTIONS[0] = EAST;
		ALL_ACTIONS[1] = NORTH;
		ALL_ACTIONS[2] = WEST;
		ALL_ACTIONS[3] = SOUTH;
		ALL_ACTIONS[4] = SAMPLE;

		// build check actions
		for (int i = 5; i < k + 5; i++) {
			int numRock = i - 5;
			ActionRock check = new ActionRock("CHECK_" + numRock + "(" + rocks.rocks.get(numRock) + ")");
			check.checkedRock = numRock;
			ALL_ACTIONS[i] = check;
		}

	}

	/**
	 * get all actions
	 * 
	 */
	static public ActionRock[] getAllActions() {
		return ALL_ACTIONS;
	}

	private ActionRock(String s) {
		super(s);
	}

}
