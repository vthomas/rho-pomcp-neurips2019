package problem.rhopomdp_belief_bprime.rockSampling;

/**
 * rock used for rock problem
 * 
 * @author vthomas
 *
 */
public class Location {

	/**
	 * rock location
	 */
	int x;
	int y;

	public Location(int x2, int y2) {
		this.x = x2;
		this.y = y2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	public String toString()
	{
		return "("+x+","+y+")";
	}


	

}
