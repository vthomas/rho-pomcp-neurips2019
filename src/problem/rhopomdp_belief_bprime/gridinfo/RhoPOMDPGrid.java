package problem.rhopomdp_belief_bprime.gridinfo;

import java.util.Arrays;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * rhoGrid Maze problem
 * 
 * <ul>
 * 
 * <li>toroidal maze described by color on each location
 * 
 * <li>when agent is moving, it may stay on its location
 * (P_TRANSITION_SLIPPING).
 * 
 * <li>after the agent has moved, it receives an observation of the color of its
 * location (possibly with noise : P_OBS_ERROR)
 * 
 * </ul>
 * 
 */

public abstract class RhoPOMDPGrid implements RhoPOMDP<StateGrid, ActionGrid, ObsGrid> {

	/**
	 * slipping probability
	 */
	protected double pTransitionStaying = 0.2;

	/**
	 * incorrect observation probability
	 */
	double pObsError = 0.;

	/**
	 * colors of maze locations
	 */
	private int[][] maze;

	/**
	 * liste des etats et des actions
	 */
	List<StateGrid> states;
	List<ActionGrid> actions;
	List<ObsGrid> obs;

	/**
	 * deterministic rhopomdp maze creation.
	 * <ul>
	 * <li>proba of slipping = 0
	 * <li>proba of incorrect obs = 0
	 * </ul>
	 * 
	 * @param tab array defining the maze
	 */
	public RhoPOMDPGrid() {
		// generate maze date
		this.maze = new int[][] { { 0, 0, 1 }, { 0, 1, 1 }, { 1, 1, 1 } };

		// list of states
		int tx = getMaze().length;
		int ty = getMaze()[0].length;
		StateGrid[] allStates = StateGrid.allStates(tx, ty);
		states = Arrays.asList(allStates);

		// actions list
		actions = Arrays.asList(ActionGrid.ACTIONS);

		// observations list
		obs = Arrays.asList(ObsGrid.OBS);
	}

	/**
	 * create rhoPOMDP with slipping probability and possibility incorrect
	 * observations.
	 * 
	 * @param tab      maze description
	 * @param slipping slipping probability during transition
	 * @param obsError incorrect observation probability
	 */
	public RhoPOMDPGrid(double slipping, double obsError) {
		this();

		// if probabilities badly given
		if ((obsError > 1) || (obsError < 0) || (slipping > 1) || (slipping < 0))
			throw new Error("problem with probabilities");

		// update probabilities
		this.pObsError = obsError;
		this.pTransitionStaying = slipping;
	}

	/************ TRANSITION, OBSERVATION AND REWARD ********************/

	@Override
	/**
	 * transition probability function. Slipping probability defines noise during
	 * transition.
	 * 
	 * <ul>
	 * <li>desired move is performed with probability (1-ERROR)
	 * <li>desired move is replaced by an orthogonal move with probability (ERROR/
	 * 2) for each direction
	 * </ul>
	 */
	public Distribution<StateGrid> transition(StateGrid s, ActionGrid action) {
		// TODO optimization - possible to compute all in a static way (to prevent
		// computer time cost)

		// transition with slipping probability
		Distribution<StateGrid> dist = new Distribution<>();

		// verify action is a move or a rest
		if ((action != ActionGrid.EAST) && (action != ActionGrid.WEST) && (action != ActionGrid.NORTH)
				&& (action != ActionGrid.SOUTH)) {
			throw new Error("unknown action");
		}

		// compute predicted state (toroidal world + action)
		StateGrid resultState = this.nextState(s, action);
		dist.addProbability(resultState, 1 - pTransitionStaying);

		// if slipping probability exists
		if (pTransitionStaying > 0) {
			dist.addProbability(s, pTransitionStaying);
		}
		return dist;
	}

	/**
	 * compute next state from state and action
	 * 
	 * @param s      starting state
	 * @param action performed action
	 * @return next state
	 */
	private StateGrid nextState(StateGrid s, ActionGrid action) {

		StateGrid nextS = new StateGrid(s.x + action.dX, s.y + action.dY);

		// toroidal world
		// move x
		int tx = this.getMaze().length;
		if (nextS.x >= tx) {
			nextS.x -= tx;
		}
		if (nextS.x < 0) {
			nextS.x += tx;
		}

		// move y
		int ty = this.getMaze()[0].length;
		if (nextS.y >= ty) {
			nextS.y -= ty;
		}
		if (nextS.y < 0) {
			nextS.y += ty;
		}

		return GeneratorGrid.getstate(nextS);
	}

	@Override
	/**
	 * reward corresponds to information on x
	 */
	abstract public double reward(Belief<StateGrid> bStart, ActionGrid action, Belief<StateGrid> bEnd);

	@Override
	/**
	 * observation function, returns the colour of the arrival state.
	 * <p>
	 * it is possible to add noise through P_OBS_ERROR constant.
	 * 
	 * @param start  state before transition
	 * @param action performed action
	 * @param end    state after transition
	 * @return distribution over possible observations
	 */
	public Distribution<ObsGrid> observation(StateGrid start, ActionGrid action, StateGrid end) {
		Distribution<ObsGrid> observs = new Distribution<ObsGrid>();

		// fetch the corresponding color from maze
		int obs = this.getMaze()[end.x][end.y];

		// add correct observation
		observs.addProbability(GeneratorGrid.getobs(obs), 1 - pObsError);

		// if noise possible, return other color
		if (pObsError > 0) {
			observs.addProbability(GeneratorGrid.getobs(1 - obs), pObsError);
		}

		return observs;
	}

	@Override
	/**
	 * list of all possible states
	 */
	public List<StateGrid> allStates() {
		return states;
	}

	@Override
	/**
	 * list of all possible actions
	 */
	public List<ActionGrid> allAction() {
		return actions;
	}

	@Override
	/**
	 * list of all possible actions
	 */
	public List<ObsGrid> allObs() {
		return this.obs;
	}

	/******************* GETTER ***********************/

	public int[][] getMaze() {
		return maze;
	}

	public void setMaze(int[][] maze) {
		this.maze = maze;
	}

	@Override
	public double getGamma() {
		return 0.95;
	}

	@Override
	public Belief<StateGrid> getB0() {
		// uniform belief
		Belief<StateGrid> unifBelief = new Belief<>();
		for (StateGrid s : this.states) {
			unifBelief.put(s, 1);
		}
		unifBelief.setUniform();

		// return initial belief
		return unifBelief;
	}

}
