package problem.rhopomdp_belief_bprime.gridinfo;

import model.mdp.ActionString;

/**
 * action for the Maze Localization problem
 * 
 */
public class ActionGrid extends ActionString {

	/**
	 * possible actions
	 */
	public static ActionGrid NORTH = new ActionGrid("NORTH", 0, -1);
	public static ActionGrid SOUTH = new ActionGrid("SOUTH", 0, 1);
	public static ActionGrid EAST = new ActionGrid("EAST", 1, 0);
	public static ActionGrid WEST = new ActionGrid("WEST", -1, 0);
	public static ActionGrid[] ACTIONS = { NORTH, SOUTH, EAST, WEST };

	/**
	 * displacment according to x and y
	 */
	int dX, dY;

	/**
	 * constructor
	 * 
	 * @param s action
	 * @param vX move in x axis
	 * @param vY move in y axis
	 * 
	 */
	private ActionGrid(String s, int vX, int vY) {
		super(s);
		this.dX = vX;
		this.dY = vY;
	}

	/**
	 * @return orthogonal vector
	 */
	public ActionGrid ortho1() {
		return new ActionGrid(null, -this.dY, this.dX);
	}

	/**
	 * @return  orthogonal vector
	 */
	public ActionGrid ortho2() {
		return new ActionGrid(null, this.dY, -this.dX);
	}

}
