package problem.rhopomdp_belief_bprime.gridinfo;

import java.util.HashMap;

/**
 * 
 * generators of objects to prevent useless duplication of references
 *
 */
public class GeneratorGrid {

	public static HashMap<StateGrid, StateGrid> states = new HashMap<>();

	public static StateGrid getstate(StateGrid s) {
		if (!states.containsKey(s))
			states.put(s, s);
		return states.get(s);
	}

	public static HashMap<Integer, ObsGrid> obs = new HashMap<>();

	public static ObsGrid getobs(int i) {
		if (!obs.containsKey(i))
			obs.put(i, new ObsGrid(i));
		return obs.get(i);
	}

}
