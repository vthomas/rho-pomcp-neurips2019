package problem.rhopomdp_belief_bprime.mazeLocalization;

import model.mdp.ActionString;

/**
 * action for the Maze Localization problem
 * 
 */
public class ActionMaze extends ActionString {

	/**
	 * possible actions
	 */
	public static ActionMaze NORTH = new ActionMaze("NORTH", 0, -1);
	public static ActionMaze SOUTH = new ActionMaze("SOUTH", 0, 1);
	public static ActionMaze EAST = new ActionMaze("EAST", 1, 0);
	public static ActionMaze WEST = new ActionMaze("WEST", -1, 0);
	public static ActionMaze REST = new ActionMaze("REST", 0, 0);
	public static ActionMaze[] ACTIONS = { NORTH, SOUTH, EAST, WEST, REST };

	/**
	 * moving according to x and y axis
	 */
	int dX, dY;

	/**
	 * constructor
	 * 
	 * @param s  action string
	 * @param vX moving in x
	 * @param vY moving in y
	 * 
	 */
	private ActionMaze(String s, int vX, int vY) {
		super(s);
		this.dX = vX;
		this.dY = vY;
	}

	/**
	 * @return orthogonal vector
	 */
	public ActionMaze ortho1() {
		return new ActionMaze(null, -this.dY, this.dX);
	}

	/**
	 * @return orthogonal vector
	 */
	public ActionMaze ortho2() {
		return new ActionMaze(null, this.dY, -this.dX);
	}

}
