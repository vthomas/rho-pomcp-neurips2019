package problem.rhopomdp_belief_bprime.mazeLocalization;

/**
 * Observation => color of a cell
 * 
 */
public class ObsMaze {

	/**
	 * observation is the color number of cell
	 */
	public int obs;

	/**
	 * add a new possible observation
	 * 
	 * @param i color number
	 */
	public ObsMaze(int i) {
		this.obs = i;
	}

	/**
	 * get all possible observations
	 */
	public static ObsMaze[] getAll(int nb) {
		ObsMaze[] obs = new ObsMaze[nb];
		for (int i = 0; i < nb; i++)
			obs[i] = new ObsMaze(i);
		return obs;
	}

	public String toString() {
		return "obs : " + this.obs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + obs;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObsMaze other = (ObsMaze) obj;
		if (obs != other.obs)
			return false;
		return true;
	}

}
