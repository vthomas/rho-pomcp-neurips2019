package problem.rhopomdp_belief_bprime.tiger;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import problem.pomdp.tiger.ActionTiger;
import problem.pomdp.tiger.ObservationTiger;
import problem.pomdp.tiger.POMDPTiger;
import problem.pomdp.tiger.StateTigre;

/**
 * build rhoPOMDP tiger from POMDP tiger
 *
 */

// TODO possible to build an Adapter
public class RhoPOMDPTiger extends POMDPTiger implements RhoPOMDP<StateTigre, ActionTiger, ObservationTiger> {

	@Override
	public double reward(Belief<StateTigre> dep, ActionTiger action, Belief<StateTigre> arr) {
		// compute reward from POMDP reward
		double reward = 0;
		for (StateTigre s : dep.keySet()) {
			reward += this.r(s, action) * dep.get(s);
		}

		return reward;
	}

	@Override
	public Distribution<ObservationTiger> observation(StateTigre starting, ActionTiger action, StateTigre fin) {
		// use pomdp observation
		return this.observe(action, fin);
	}

	@Override
	public Belief<StateTigre> getB0() {
		Belief<StateTigre> b = new Belief<StateTigre>();
		b.put(StateTigre.RIGHT, 0.5);
		b.put(StateTigre.LEFT, 0.5);
		return b;
	}

}
