package problem.rhopomdp_belief_bprime;

import model.rhopomdp_reward_bprime.RhoPOMDP;
import problem.rhopomdp_belief_bprime.activeLoc.RhoPOMDPActiveLoc;
import problem.rhopomdp_belief_bprime.cameraClean.RhoPOMDPCameraDiag;
import problem.rhopomdp_belief_bprime.gridinfo.RhoGridNotX;
import problem.rhopomdp_belief_bprime.gridinfo.RhoGridX;
import problem.rhopomdp_belief_bprime.mazeLocalization.MazeFactory;
import problem.rhopomdp_belief_bprime.mazeLocalization.RhoPOMDPMaze_withoutPrecomputatioon;
import problem.rhopomdp_belief_bprime.rockSampling.RhoPOMDPRockSample;
import problem.rhopomdp_belief_bprime.seekNseek.RhoPOMDPSeek;
import problem.rhopomdp_belief_bprime.tiger.RhoPOMDPTiger;
import problem.rhopomdp_belief_bprime.trackingMuseum.RhoPOMDPMuseum;
import problem.rhopomdp_belief_bprime.trackingMuseum.RhoPOMDPMuseumThreshold;

public class RhoPOMDP_BPrime_Factory {

	@SuppressWarnings("rawtypes")
	public static RhoPOMDP getProblem(String problemName) {
		RhoPOMDP problem;

		switch (problemName) {

		case "activLocLines_bprime":
		case "activlocLines_bprime":
		case "activloclines_bprime":
		case "activLocines_bprime":
		case "activloc_lines_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("lines"));
			break;

		case "activLocCross_bprime":
		case "activlocCross_bprime":
		case "activloccross_bprime":
		case "activLoccross_bprime":
		case "activloc_cross_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("cross"));
			break;

		case "activLocDots_bprime":
		case "activlocDots_bprime":
		case "activlocdots_bprime":
		case "activLocdots_bprime":
		case "activloc_dots_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("dots"));
			break;

		case "activLocHole_bprime":
		case "activLochole_bprime":
		case "activlocHole_bprime":
		case "activlochole_bprime":
		case "activloc_hole_bprime":
			problem = new RhoPOMDPActiveLoc(MazeFactory.maze("hole"));
			break;

		case "seek_bprime":
			problem = new RhoPOMDPSeek();
			break;

		case "mazelines_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("lines"));
			break;

		case "mazecross_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("cross"));
			break;

		case "mazeempty_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("empty"));
			break;

		case "mazehole_bprime":
		case "mazeHole_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("hole"));
			break;

		case "mazedots_bprime":
		case "mazeDots_bprime":
			problem = new RhoPOMDPMaze_withoutPrecomputatioon(MazeFactory.maze("dots"));
			break;

		case "camera":
			problem = new RhoPOMDPCameraDiag(4);
			break;

		case "tiger_bprime":
			problem = new RhoPOMDPTiger();
			break;

		case "rockSampling44_bprime":
			problem = new RhoPOMDPRockSample(4, 4, 100);
			break;

		case "museum_bprime":
			problem = new RhoPOMDPMuseum(4, 4);
			break;

		case "museum_threshold_08":
		case "museumThreshold_08":
		case "MuseumThreshold_08":
			problem = new RhoPOMDPMuseumThreshold(4, 4, 0.8);
			break;

		case "rockSampling66":
			problem = new RhoPOMDPRockSample(6, 6, 100);
			break;

		case "gridnotx_bprime":
		case "gridnotX_bprime":
		case "gridNotX_bprime":
			problem = new RhoGridNotX();
			break;

		case "gridx_bprime":
			problem = new RhoGridX();
			break;

		default:
			problem = null;
			break;
		}
		return problem;
	}

}
