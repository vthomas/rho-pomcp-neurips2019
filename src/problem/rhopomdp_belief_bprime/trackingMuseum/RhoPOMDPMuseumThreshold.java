package problem.rhopomdp_belief_bprime.trackingMuseum;

import java.util.ArrayList;
import java.util.List;

import model.mdp.Distribution;
import model.pomdp.Belief;
import model.rhopomdp_reward_bprime.RhoPOMDP;

/**
 * in museum problem,
 * <ul>
 * <li>one child is roaming in a museum according to a stochastic strategy.
 * <li>the agent can activate a camera on each cell which gives information
 * about the location of the child
 * <li>the reward is the difference of entropy of the probability over the
 * child's location
 * </ul>
 * 
 * in other words,
 * <ul>
 * <li>world = grid n x n
 * <li>state = location of child
 * <li>action = location of activated camera
 * <li>observation = Present , Absent, Close (next to the activated camera)
 * <li>state transition = child movement (0.6 staying, 0.1 on each direction)
 * <li>observation process = deterministic
 * </ul>
 *
 */
public class RhoPOMDPMuseumThreshold extends RhoPOMDPMuseum {

	/**
	 * just a threshold on belief
	 */
	double threshold;

	/////////////////// BUILDER RhoPOMDP Museum ///////////////////////

	/**
	 * creates a museum
	 * 
	 * @param n size of museum
	 */
	public RhoPOMDPMuseumThreshold(int tx, int ty, double threshold) {
		super(tx, ty);
		this.threshold = threshold;
	}

	////////////// rho POMDP reward ///////////////////////////////////////////

	@Override
	public double reward(Belief<MuseumState> bStart, MuseumAction action, Belief<MuseumState> bEnd) {
		for (MuseumState s : bEnd.keySet()) {
			if (bEnd.get(s) >= this.threshold)
				return 1.0;
		}
		return 0;
	}

}
