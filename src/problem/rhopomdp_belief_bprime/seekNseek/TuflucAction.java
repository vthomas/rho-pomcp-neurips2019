package problem.rhopomdp_belief_bprime.seekNseek;

import java.util.ArrayList;
import java.util.List;

import model.mdp.ActionString;

/**
 * action for tufluc
 *
 */
public class TuflucAction extends ActionString {

	public static TuflucAction N = new TuflucAction("NORTH");
	public static TuflucAction S = new TuflucAction("SOUTH");
	public static TuflucAction E = new TuflucAction("EAST");
	public static TuflucAction W = new TuflucAction("WEST");
	public static List<TuflucAction> ALL_ACTIONS = createList();

	private TuflucAction(String s) {
		super(s);
	}

	private static List<TuflucAction> createList() {
		List<TuflucAction> actions = new ArrayList<>();
		actions.add(N);
		actions.add(S);
		actions.add(E);
		actions.add(W);
		return actions;
	}

}
