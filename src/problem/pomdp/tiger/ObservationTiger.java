package problem.pomdp.tiger;

import model.pomdp.ObsString;

/**
 * possible observations for tiger problem
 * 
 * @author vthomas
 * 
 */
public class ObservationTiger extends ObsString {

	/**
	 * constant used
	 */
	public static ObservationTiger NOISE_RIGHT = new ObservationTiger("N_R");
	public static ObservationTiger NOISE_LEFT = new ObservationTiger("N_L");
	public static ObservationTiger[] OBS = { NOISE_RIGHT, NOISE_LEFT };

	/**
	 * private constructor
	 * 
	 * @param obsName observation name
	 */
	private ObservationTiger(String obsName) {
		super(obsName);
	}

	/**
	 * @return set of possible observations
	 */
	public ObservationTiger[] observations() {
		return OBS;
	}

}