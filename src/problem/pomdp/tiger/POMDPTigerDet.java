package problem.pomdp.tiger;

import model.mdp.Distribution;

/**
 * Tiger problem with deterministic observation
 * 
 */
public class POMDPTigerDet extends POMDPTiger {

	/**
	 * observation function
	 */
	@Override
	public Distribution<ObservationTiger> observe(ActionTiger action, StateTigre fin) {
		// if opening door, any observation
		if ((action == ActionTiger.OPEN_LEFT) || (action == ActionTiger.OPEN_RIGHT)) {
			Distribution<ObservationTiger> dist = new Distribution<>();
			dist.addProbability(ObservationTiger.NOISE_RIGHT, 1.);
			return (dist);
		}

		// if listening
		if (action == ActionTiger.LISTEN) {
			// if tiger left
			double correctProbability = 1.;
			if (fin.equals(StateTigre.LEFT)) {
				// tiger left
				Distribution<ObservationTiger> dist = new Distribution<ObservationTiger>();
				dist.addProbability(ObservationTiger.NOISE_LEFT, correctProbability);
				dist.addProbability(ObservationTiger.NOISE_RIGHT, 1 - correctProbability);
				return dist;
			} else {
				// tiger right
				Distribution<ObservationTiger> dist = new Distribution<ObservationTiger>();
				dist.addProbability(ObservationTiger.NOISE_LEFT, 1 - correctProbability);
				dist.addProbability(ObservationTiger.NOISE_RIGHT, correctProbability);
				return dist;
			}
		}

		// problem unknown action
		throw new Error("unknown action");
	}

}
