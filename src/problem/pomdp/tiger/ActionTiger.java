package problem.pomdp.tiger;

import model.mdp.ActionString;

/**
 * possible actions for tiger problem
 * 
 * @author vthomas
 * 
 */
public class ActionTiger extends ActionString {

	public static ActionTiger LISTEN = new ActionTiger("Listen");
	public static ActionTiger OPEN_LEFT = new ActionTiger("OpenLeft");
	public static ActionTiger OPEN_RIGHT = new ActionTiger("OpenRight");
	public static ActionTiger[] ACTIONS = { LISTEN, OPEN_LEFT, OPEN_RIGHT };

	/**
	 * private creation
	 * 
	 * @param s action name
	 */
	private ActionTiger(String s) {
		super(s);
	}

}
