
import algo.rhopomdp_reward_bprime.AlgoSimulate;
import algo.rhopomdp_reward_bprime.ExecuteRhoPOMDP;
import algo.rhopomdp_reward_bprime.myopic.ExecuteMyopicSearch;
import algo.rhopomdp_reward_bprime.random.ExecRhoPOMDPRandom;
import algo.rhopomdp_reward_bprime.rhoBeliefUCT.*;
import algo.rhopomdp_reward_bprime.rhoBeliefUCT.rollout.*;
import algo.rhopomdp_reward_bprime.rhopomcp.*;
import algo.rhopomdp_reward_bprime.rhopomcp.beliefGen.*;
import algo.rhopomdp_reward_bprime.rhopomcp.rollout.*;
import algo.rhopomdp_reward_bprime.rhopomcp.valueEstimator.*;
import algo.rhopomdp_reward_bprime.tree.*;
import model.rhopomdp_reward_bprime.RhoPOMDP;
import problem.rhopomdp_belief_bprime.RhoPOMDP_BPrime_Factory;
import tools.RandomGen;
import tools.options.AnalyseurOptions;
import tools.options.Options;

/**
 * launch rhoPOMDP simulation
 *
 */
public class MainRhoPOMDP_rewardBprime {

	/**
	 * launch rho pomdp simulation by analysing args
	 *
	 * @param args
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {

		// build available options
		buildOptions();

		try {
			// parse options
			Options option = Options.getOptions();
			AnalyseurOptions analyse = new AnalyseurOptions(option);
			analyse.analyser(args);

			// choice of problem
			String pb = option.getValString("pb");
			RhoPOMDP problem = RhoPOMDP_BPrime_Factory.getProblem(pb);
			if (problem == null)
				option.error("unknown problem '" + pb + "'");

			// choice of algorithm
			ExecuteRhoPOMDP execAlgo = null;
			String algo = option.getValString("algo");
			switch (algo) {

			case "tree":
				int depth = option.getValInt("depth");
				execAlgo = new ExecRhoTree(problem, depth);
				break;

			case "rhopomcp":
			case "rhoPOMCP":
				execAlgo = createRhoPOMCP(option, problem);
				break;

			case "rhopomcptime":
			case "rhopomcpTime":
			case "rhoPOMCPTime":
				execAlgo = createRhoPOMCPTime(option, problem);
				break;

			case "rhobeliefuct":
			case "rhobeliefUCT":
			case "rhoBeliefUCT":
				execAlgo = createRhoBeliefUct(option, problem);
				break;

			case "random":
				execAlgo = new ExecRhoPOMDPRandom<>(problem);
				break;

			case "myopic":
				execAlgo = new ExecuteMyopicSearch(problem);
				break;

			default:
				option.error("unknown algorithm '" + algo + "'");
			}

			// launch simulation
			RandomGen.newSeed();
			AlgoSimulate simulation = new AlgoSimulate<>(execAlgo, problem);
			int actions = option.getValInt("actions");
			int episodes = option.getValInt("episodes");
			simulation.launchSimulations(episodes, actions, problem.getB0());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * builds available options
	 */
	private static void buildOptions() {
		// get option object
		Options option = Options.getOptions();

		// problem option
		option.setOptions("Problem", "pb", "XXX", "Problem used ('camera' or 'maze' or 'tiger' or 'rockSampling44')");
		option.setOptions("Problem", "algo", "rhopomcp",
				"Algorithm used ('tree', 'rhopomcp', 'rhopomcptime','random' or 'myopic')");

		// simulation options
		option.setOptions("Simulation", "actions", 30, "number of actions by episode");
		option.setOptions("Simulation", "episodes", 100, "number of episodes");

		// add rhoPOMCP options
		option.setOptions("Algo_RhoPOMCP", "ucb", 10., "UCB constant for POMCP approaches");
		option.setOptions("Algo_RhoPOMCP", "beta", 100, "number of particles for belief generation");
		option.setOptions("Algo_RhoPOMCP", "visitThreshold", -1, "number of particles for adpative deblief generation");

		option.setOptions("Algo_RhoPOMCP", "descents", 1000, "number of descents foreach iteration");
		option.setOptions("Algo_RhoPOMCP", "time", 1000, "time budget foreach iteration");
		option.setOptions("Algo_RhoPOMCP", "beliefGen", "XXX",
				"way to generate belief in belief tree ('reject' or 'pfilter' or 'pfadpative')");
		option.setOptions("Algo_RhoPOMCP", "rollout", "XXX",
				"rollout ('myopic' or 'myopicOne' or 'random' or 'constant-XXX' with double XXX)");
		option.setOptions("Algo_RhoPOMCP", "update", "XXX",
				"rollout ('normal' (POMCP version) or 'reward' (reward estimate) or 'value' (value estimate)");

		// add tree options
		option.setOptions("Algo_Tree", "depth", 4, "Depth used when full recursive evaluation");
	}

	////// CREATES ALGORITHMS /////////////////////////

	/**
	 * create a rho belief UCT problem
	 * 
	 * @param option
	 * @param problem
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ExecuteRhoPOMDP createRhoBeliefUct(Options option, RhoPOMDP problem) {
		ExecuteRhoPOMDP execAlgo;

		// get rollout
		RolloutBeliefUCT rollout = getRolloutUCT(option, problem, null);

		// get rhopomcp options
		int descents = option.getValInt("descents");
		double ucb = option.getValDouble("ucb");

		// create algorithm
		execAlgo = new ExecuteRhoBelief(problem, rollout, ucb, descents);
		return execAlgo;
	}

	@SuppressWarnings("rawtypes")
	private static RolloutBeliefUCT getRolloutUCT(Options option, RhoPOMDP problem, Object object) {
		RolloutBeliefUCT rollout = null;
		String stringRollout = option.getValString("rollout");
		// if rollout non specified
		if (stringRollout.equals("XXX")) {
			throw new Error("rollout non specified");
		}
		// random rollout
		else if (stringRollout.equals("random")) {
			rollout = new RolloutBeliefRandom();
		}
		// constant rollout
		else {
			String constantPrefix = "constant";
			String prefix = stringRollout.substring(0, constantPrefix.length());
			if (prefix.equals("constant")) {
				double value = Double.parseDouble(stringRollout.split("-")[1]);
				rollout = new RolloutBeliefConstant(value);
			}
		}
		return rollout;
	}

	/**
	 * creates rhopomcp algorithm
	 *
	 * @param option  known options
	 * @param problem problem to solve
	 * @return rhopomcp algo
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ExecuteRhoPOMDP createRhoPOMCP(Options option, RhoPOMDP problem) {
		ExecuteRhoPOMDP execAlgo;

		// get beliefgenerator
		BeliefGeneration beliefGen = getBeliefGen(option, problem);

		// get update
		ValueEstimate update = getValueEstimate(option);

		// get rollout
		Rollout rollout = getRollout(option, problem, beliefGen);

		// get rhopomcp options
		int descents = option.getValInt("descents");
		double ucb = option.getValDouble("ucb");

		// create algorithm
		execAlgo = new ExecuteRhoPOMCP(problem, beliefGen, rollout, update, descents, ucb, true);
		return execAlgo;
	}

	/**
	 * get the way value is re-estimated during backpropagation
	 * 
	 * @param option  the main option
	 * @param problem the problem to solve
	 * @return the way value is estimated
	 */
	@SuppressWarnings("rawtypes")
	private static ValueEstimate getValueEstimate(Options option) {
		// get option name
		String estimateName = option.getValString("update");

		// estimate to return
		ValueEstimate estimate = null;

		// get corresponding estimate
		switch (estimateName) {
		// no update specified
		case "XXX":
			option.error("update (='XXX') non specified");
			break;

		// classical update
		case "normal":
			estimate = new POMCPEstimate();
			break;

		// update with reward re-estimation with rho(obs)
		case "rewardobs":
			estimate = new RewardUpdateEstimateObs();
			break;

		// update with value re-estimation with rho(obs)
		case "valueobs":
			estimate = new ValueUpdateObs();
			break;

		default:
			option.error("unknown estimator'" + estimateName + "'");
			break;

		}

		return estimate;
	}

	/**
	 * get belief generator
	 * 
	 * @param option  options used
	 * @param problem problem to solev
	 * @return belief generator
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static BeliefGeneration getBeliefGen(Options option, RhoPOMDP problem) {
		// get number of new particles by descent (minus the considered one)
		int betaSize = option.getValInt("beta");

		// define belief generator
		String beliefG = option.getValString("beliefGen");
		// generation procedure
		BeliefGeneration beliefGen = null;
		switch (beliefG) {
		// beliefgen non specified
		case "XXX":
			option.error("beliefGen (='XXX') non specified");
			break;
		// belief gen by reject
		case "reject":
			beliefGen = new BeliefGenerationReject(problem, betaSize);
			break;
		// belief gen by particle filter
		case "pfilter":
			beliefGen = new BeliefGenerationParticles(problem, betaSize);
			break;

		// belief gen by particle filter adaptive beta
		case "pfadaptive":
			int visits = option.getValInt("visitThreshold");
			if (visits == -1)
				throw new Error("visit threshold must be given for pfadaptive");
			beliefGen = new BeliefGenerationAdaptiveBag(problem, betaSize, visits);
			break;

		default:
			option.error("unknown belief generator '" + beliefG + "'");
		}
		return beliefGen;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ExecuteRhoPOMDP createRhoPOMCPTime(Options option, RhoPOMDP problem) {
		ExecuteRhoPOMDP execAlgo;

		// get beliefgenerator
		BeliefGeneration beliefGen = getBeliefGen(option, problem);

		// rollout option
		Rollout rollout = getRollout(option, problem, beliefGen);

		// estimate option
		ValueEstimate estimate = getValueEstimate(option);

		// get rhopomcp options
		int timeBudget = option.getValInt("time");
		double ucb = option.getValDouble("ucb");

		// create algorithm
		execAlgo = new ExecuteRhoPOMCPTime(problem, beliefGen, rollout, estimate, timeBudget, ucb, true);
		return execAlgo;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Rollout getRollout(Options option, RhoPOMDP pb, BeliefGeneration beliefGen) throws Error {
		Rollout rollout = null;
		String stringRollout = option.getValString("rollout");
		// if rollout non specified
		if (stringRollout.equals("XXX")) {
			throw new Error("rollout non specified");
		}
		// random rollout
		else if (stringRollout.equals("random")) {
			rollout = new RolloutRandom(beliefGen);
		}
		// myopic rollout
		else if (stringRollout.equals("myopic")) {
			rollout = new RolloutMyopic(pb, beliefGen);
		} else if (stringRollout.equals("myopicOne")) {
			rollout = new RolloutMyopicOne(pb, beliefGen);
		}
		// constant rollout
		else {
			String constantPrefix = "constant";
			String prefix = stringRollout.substring(0, constantPrefix.length());
			if (prefix.equals("constant")) {
				double value = Double.parseDouble(stringRollout.split("-")[1]);
				rollout = new RolloutConstant(value);
			}
		}
		return rollout;
	}

}
