## Content

This repository contains class used for the "Monte Carlo Information-Oriented Planning" article proposed at neurips2019 by Vincent Thomas, Gérémy Hutin and Olivier Buffet. These classes have been written by Vincent Thomas.

This repository proposes

- interfaces for defining MDP, POMDP, and rho-POMDP in the ```model``` package;
- the proposed algorithms belief look-ahead, rhoBeliefUCT and rhoPOMCP in the ```algo``` package;
- the problems used for assessment in the ```problems``` package.


## Compilation

Compiling does not require any specific library and consists in compiling the ```Main``` class.

```
javac MainRhoPOMDP_rewardBprime.java
```

## Help

Help is accessible through --help option

```
java MainRhoPOMDP_rewardBprime --help
```

## Execution

All parameters are given to the main program via options of the shape ```-param=value``` where

- *param* denotes the name of the parameter
- *value* denotes its value

To be executed, the main program requires

- the name of the used algorithm with the help of ```-algo``` option
- the name of the test problem  with the help of ```-pb``` option

**Possible algorithms** are

1. ```random``` for random policy execution
2. ```tree``` for complete dynamic programming on a receding horizon
3. ```rhoPOMCP``` for RhoPOMCP with a fixed number of descents
4. ```rhoPOMCPTime``` for RhoPOMCP with a fixed time budget
5. ```rhoBeliefUCT``` for RhobeliefUCT with a fixed number of descents

**Possible problems** are

1. ```camera``` for cameraclean
2. ```tiger_bprime``` for tiger problem
3. ```seek_bprime``` for seek and seek problem
4. ```activLocX_bprime``` for active localization problem, where X can be ```Lines```, ```Cross```, ```Dots``` or ```Hole```
5. ```museum_bprime``` for museum problem or ```MuseumThreshold_08``` for museum problem with threshold rewards
6. ```gridNotX_bprime```or ```gridx_bprime``` for gridNotX and gridX problem

For instance,
```
java MainRhoPOMDP_rewardBprime -pb=gridNotX_bprime -algo=rhoPOMCP -beliefGen=pfilter -update=normal -rollout=constant-0
```
will execute rhoPOMCP on the gridnotX problem with several rhoPOMCP parameters.

## Parameters for each algorithm

### Global Parameters

Whenever the main program is executed, it will execute *episodes* runs of *actions* successive actions. By default, it will execute 100 runs of 30 successive actions. But this can be changed by using

- ```-episodes=XXX``` option where XX represents the number of runs;
- ```-actions=XXX``` option where XX represents the number of actions for each run.


### *random* algorithm

Random algorithm does not require any parameter and consists to execute a random action at each time step.

```
java MainRhoPOMDP_rewardBprime -pb=gridNotX_bprime -algo=random
```

### *tree* algorithm

Tree algorithm consists in performing dynamic programming with a finite receding horizon. The launch of this algorithm can be modified with options

- ```-episodes=XXX```
- ```-actions=XXX```
- ```-depth=XXX``` where XXX represents the horizon to consider for dynamic programming

```
java MainRhoPOMDP_rewardBprime -pb=tiger_bprime -algo=tree -depth=5 -actions=20 -episodes=10
```

### *rhoPOMCP* algorithm

rhoPOMCP algorithm consists in executing the rhoPOMCP algorithm presented in the article with a fixed number of descents.

Several arguments are required to launch the rhoPOMCP algorithm

- UCB configuration
    - ```-ucb=XXX``` where XXX denotes the ucb exploration constant
    - ```-descents=XXX``` where XXX denotes the number of rhoPOMCP descents before executing an action
- Particle filter configuration
    - ```-beliefGen=XXX``` where XXX denotes the filter used for particles propagation ('pfilter' for weighted particle filter or 'reject' for a rejection approach)
    - ```-beta=XXX``` where XXX denotes the size of the small bags used at each descent
- Variants
    - ```-rollout=XXX``` where XXX denotes the rollout used (which can be 'random', 'myopic' or 'constant-XXX' with XXX the default double return value)
    - ```-update=XXX``` where XXX denotes the way the value is updated at each belief node (it can be 'normal' for POMCP evaluation, 'reward' for last-reward-update or 'value' for last-value-update).

```
java MainRhoPOMDP_rewardBprime -pb=gridnotx_bprime -algo=rhoPOMCP -actions=20 -episodes=10 -ucb=26 -descents=10000 -beliefGen=pfilter -beta=50 -rollout=constant-0 -update=normal
```

### *rhoPOMCPTime* algorithm

rhoPOMCPTime executes a rhoPOMCP algorithm with a fixed time budget (in ms). The arguments given to the main program are the same as the rhoPOMCP arguments except that rhoPOMCPTime does not require a ```descent``` parameter but a ```time``` parameter.

- ```-time=XXX``` where XXX denotes the time-budget (in ms) given to rhoPOMCP before performing an action

```
java MainRhoPOMDP_rewardBprime -pb=gridnotx_bprime -algo=rhoPOMCPTime -actions=20 -episodes=10 -ucb=26 -time=500 -beliefGen=pfilter -beta=50 -rollout=constant-0 -update=normal
```

### *rhoBeliefUCT* algorithm

rhoBeliefUCT executes the rho-BeliefUCT presented in the article (which consists in applying directly UCT on the belief MDP - by computing exact beliefstate of the new node added to the tree after each descent).

It requires the following parameters :

- UCB configuration
    - ```-ucb=XXX``` where XXX denotes the ucb exploration constant
    - ```-descents=XXX``` where XXX denotes the number of rhoBeliefUCT descents before executing an action
- ```-rollout=XXX``` where XXX denotes the rollout used (which can be 'random', 'myopic' or 'constant-XXX' with XXX the default double return value)

```
java MainRhoPOMDP_rewardBprime -pb=gridnotx_bprime -algo=rhoBeliefUCT -rollout=constant-0 -ucb=26 -descents=10000
```

## Produced output

Whenever an algorithm is launched, the main program print a CVS formatted string with several informations depending on the launched algorithm.

- **num** column corresponds to the number of the current run.
- **initialS** column corresponds to the initial true state sampled for the initial belief (to gather runs by sampled initial state if needed).
- **performance** column corresponds to the cumulated discounted reward obtained by the execution of this run.
- **duration(ms)** column corresponds to the duration of the run.
- **nbDescents** column corresponds to the number of descents of the first descent.
- **XactionX** columns (where XactionX corresponds to an action) correspond to the number of visits of the first action during the first descent.
- **seed** column corresponds to the seed used when launching the first run (it is not updated in the following runs).
